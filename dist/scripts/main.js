/******/ (function(modules) { // webpackBootstrap
	/******/ 	// The module cache
	/******/ 	var installedModules = {};
	/******/
	/******/ 	// The require function
	/******/ 	function __webpack_require__(moduleId) {
		/******/
		/******/ 		// Check if module is in cache
		/******/ 		if(installedModules[moduleId]) {
			/******/ 			return installedModules[moduleId].exports;
			/******/ 		}
		/******/ 		// Create a new module (and put it into the cache)
		/******/ 		var module = installedModules[moduleId] = {
			/******/ 			i: moduleId,
			/******/ 			l: false,
			/******/ 			exports: {}
			/******/ 		};
		/******/
		/******/ 		// Execute the module function
		/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
		/******/
		/******/ 		// Flag the module as loaded
		/******/ 		module.l = true;
		/******/
		/******/ 		// Return the exports of the module
		/******/ 		return module.exports;
		/******/ 	}
	/******/
	/******/
	/******/ 	// expose the modules object (__webpack_modules__)
	/******/ 	__webpack_require__.m = modules;
	/******/
	/******/ 	// expose the module cache
	/******/ 	__webpack_require__.c = installedModules;
	/******/
	/******/ 	// define getter function for harmony exports
	/******/ 	__webpack_require__.d = function(exports, name, getter) {
		/******/ 		if(!__webpack_require__.o(exports, name)) {
			/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
			/******/ 		}
		/******/ 	};
	/******/
	/******/ 	// define __esModule on exports
	/******/ 	__webpack_require__.r = function(exports) {
		/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
			/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
			/******/ 		}
		/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
		/******/ 	};
	/******/
	/******/ 	// create a fake namespace object
	/******/ 	// mode & 1: value is a module id, require it
	/******/ 	// mode & 2: merge all properties of value into the ns
	/******/ 	// mode & 4: return value when already ns object
	/******/ 	// mode & 8|1: behave like require
	/******/ 	__webpack_require__.t = function(value, mode) {
		/******/ 		if(mode & 1) value = __webpack_require__(value);
		/******/ 		if(mode & 8) return value;
		/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
		/******/ 		var ns = Object.create(null);
		/******/ 		__webpack_require__.r(ns);
		/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
		/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
		/******/ 		return ns;
		/******/ 	};
	/******/
	/******/ 	// getDefaultExport function for compatibility with non-harmony modules
	/******/ 	__webpack_require__.n = function(module) {
		/******/ 		var getter = module && module.__esModule ?
			/******/ 			function getDefault() { return module['default']; } :
			/******/ 			function getModuleExports() { return module; };
		/******/ 		__webpack_require__.d(getter, 'a', getter);
		/******/ 		return getter;
		/******/ 	};
	/******/
	/******/ 	// Object.prototype.hasOwnProperty.call
	/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
	/******/
	/******/ 	// __webpack_public_path__
	/******/ 	__webpack_require__.p = "/wp-content/themes/canpar/dist/";
	/******/
	/******/
	/******/ 	// Load entry module and return exports
	/******/ 	return __webpack_require__(__webpack_require__.s = 0);
	/******/ })
	/************************************************************************/
	/******/ ({

		/***/ "../node_modules/jquery-ui/ui/core.js":
		/*!********************************************!*\
  !*** ../node_modules/jquery-ui/ui/core.js ***!
  \********************************************/
		/*! no static exports found */
		/***/ (function(module, exports, __webpack_require__) {

			var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_RESULT__;// This file is deprecated in 1.12.0 to be removed in 1.13
			( function() {
				!(__WEBPACK_AMD_DEFINE_FACTORY__ = ([
					"jquery",
					"./data",
					"./disable-selection",
					"./focusable",
					"./form",
					"./ie",
					"./keycode",
					"./labels",
					"./jquery-1-7",
					"./plugin",
					"./safe-active-element",
					"./safe-blur",
					"./scroll-parent",
					"./tabbable",
					"./unique-id",
					"./version"
				]),
					__WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
						(__WEBPACK_AMD_DEFINE_FACTORY__.call(exports, __webpack_require__, exports, module)) :
						__WEBPACK_AMD_DEFINE_FACTORY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
			} )();


			/***/ }),

		/***/ "../node_modules/jquery-ui/ui/effect.js":
		/*!**********************************************!*\
  !*** ../node_modules/jquery-ui/ui/effect.js ***!
  \**********************************************/
		/*! no static exports found */
		/***/ (function(module, exports, __webpack_require__) {

			var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;/*!
 * jQuery UI Effects 1.12.1
 * http://jqueryui.com
 *
 * Copyright jQuery Foundation and other contributors
 * Released under the MIT license.
 * http://jquery.org/license
 */

//>>label: Effects Core
//>>group: Effects
// jscs:disable maximumLineLength
//>>description: Extends the internal jQuery effects. Includes morphing and easing. Required by all other effects.
// jscs:enable maximumLineLength
//>>docs: http://api.jqueryui.com/category/effects-core/
//>>demos: http://jqueryui.com/effect/

			( function( factory ) {
				if ( true ) {

					// AMD. Register as an anonymous module.
					!(__WEBPACK_AMD_DEFINE_ARRAY__ = [ __webpack_require__(/*! jquery */ "jquery"), __webpack_require__(/*! ./version */ "../node_modules/jquery-ui/ui/version.js") ], __WEBPACK_AMD_DEFINE_FACTORY__ = (factory),
						__WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
							(__WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)) : __WEBPACK_AMD_DEFINE_FACTORY__),
					__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
				} else {}
			}( function( $ ) {

				var dataSpace = "ui-effects-",
					dataSpaceStyle = "ui-effects-style",
					dataSpaceAnimated = "ui-effects-animated",

					// Create a local jQuery because jQuery Color relies on it and the
					// global may not exist with AMD and a custom build (#10199)
					jQuery = $;

				$.effects = {
					effect: {}
				};

				/*!
 * jQuery Color Animations v2.1.2
 * https://github.com/jquery/jquery-color
 *
 * Copyright 2014 jQuery Foundation and other contributors
 * Released under the MIT license.
 * http://jquery.org/license
 *
 * Date: Wed Jan 16 08:47:09 2013 -0600
 */
				( function( jQuery, undefined ) {

					var stepHooks = "backgroundColor borderBottomColor borderLeftColor borderRightColor " +
						"borderTopColor color columnRuleColor outlineColor textDecorationColor textEmphasisColor",

						// Plusequals test for += 100 -= 100
						rplusequals = /^([\-+])=\s*(\d+\.?\d*)/,

						// A set of RE's that can match strings and generate color tuples.
						stringParsers = [ {
							re: /rgba?\(\s*(\d{1,3})\s*,\s*(\d{1,3})\s*,\s*(\d{1,3})\s*(?:,\s*(\d?(?:\.\d+)?)\s*)?\)/,
							parse: function( execResult ) {
								return [
									execResult[ 1 ],
									execResult[ 2 ],
									execResult[ 3 ],
									execResult[ 4 ]
								];
							}
						}, {
							re: /rgba?\(\s*(\d+(?:\.\d+)?)\%\s*,\s*(\d+(?:\.\d+)?)\%\s*,\s*(\d+(?:\.\d+)?)\%\s*(?:,\s*(\d?(?:\.\d+)?)\s*)?\)/,
							parse: function( execResult ) {
								return [
									execResult[ 1 ] * 2.55,
									execResult[ 2 ] * 2.55,
									execResult[ 3 ] * 2.55,
									execResult[ 4 ]
								];
							}
						}, {

							// This regex ignores A-F because it's compared against an already lowercased string
							re: /#([a-f0-9]{2})([a-f0-9]{2})([a-f0-9]{2})/,
							parse: function( execResult ) {
								return [
									parseInt( execResult[ 1 ], 16 ),
									parseInt( execResult[ 2 ], 16 ),
									parseInt( execResult[ 3 ], 16 )
								];
							}
						}, {

							// This regex ignores A-F because it's compared against an already lowercased string
							re: /#([a-f0-9])([a-f0-9])([a-f0-9])/,
							parse: function( execResult ) {
								return [
									parseInt( execResult[ 1 ] + execResult[ 1 ], 16 ),
									parseInt( execResult[ 2 ] + execResult[ 2 ], 16 ),
									parseInt( execResult[ 3 ] + execResult[ 3 ], 16 )
								];
							}
						}, {
							re: /hsla?\(\s*(\d+(?:\.\d+)?)\s*,\s*(\d+(?:\.\d+)?)\%\s*,\s*(\d+(?:\.\d+)?)\%\s*(?:,\s*(\d?(?:\.\d+)?)\s*)?\)/,
							space: "hsla",
							parse: function( execResult ) {
								return [
									execResult[ 1 ],
									execResult[ 2 ] / 100,
									execResult[ 3 ] / 100,
									execResult[ 4 ]
								];
							}
						} ],

						// JQuery.Color( )
						color = jQuery.Color = function( color, green, blue, alpha ) {
							return new jQuery.Color.fn.parse( color, green, blue, alpha );
						},
						spaces = {
							rgba: {
								props: {
									red: {
										idx: 0,
										type: "byte"
									},
									green: {
										idx: 1,
										type: "byte"
									},
									blue: {
										idx: 2,
										type: "byte"
									}
								}
							},

							hsla: {
								props: {
									hue: {
										idx: 0,
										type: "degrees"
									},
									saturation: {
										idx: 1,
										type: "percent"
									},
									lightness: {
										idx: 2,
										type: "percent"
									}
								}
							}
						},
						propTypes = {
							"byte": {
								floor: true,
								max: 255
							},
							"percent": {
								max: 1
							},
							"degrees": {
								mod: 360,
								floor: true
							}
						},
						support = color.support = {},

						// Element for support tests
						supportElem = jQuery( "<p>" )[ 0 ],

						// Colors = jQuery.Color.names
						colors,

						// Local aliases of functions called often
						each = jQuery.each;

// Determine rgba support immediately
					supportElem.style.cssText = "background-color:rgba(1,1,1,.5)";
					support.rgba = supportElem.style.backgroundColor.indexOf( "rgba" ) > -1;

// Define cache name and alpha properties
// for rgba and hsla spaces
					each( spaces, function( spaceName, space ) {
						space.cache = "_" + spaceName;
						space.props.alpha = {
							idx: 3,
							type: "percent",
							def: 1
						};
					} );

					function clamp( value, prop, allowEmpty ) {
						var type = propTypes[ prop.type ] || {};

						if ( value == null ) {
							return ( allowEmpty || !prop.def ) ? null : prop.def;
						}

						// ~~ is an short way of doing floor for positive numbers
						value = type.floor ? ~~value : parseFloat( value );

						// IE will pass in empty strings as value for alpha,
						// which will hit this case
						if ( isNaN( value ) ) {
							return prop.def;
						}

						if ( type.mod ) {

							// We add mod before modding to make sure that negatives values
							// get converted properly: -10 -> 350
							return ( value + type.mod ) % type.mod;
						}

						// For now all property types without mod have min and max
						return 0 > value ? 0 : type.max < value ? type.max : value;
					}

					function stringParse( string ) {
						var inst = color(),
							rgba = inst._rgba = [];

						string = string.toLowerCase();

						each( stringParsers, function( i, parser ) {
							var parsed,
								match = parser.re.exec( string ),
								values = match && parser.parse( match ),
								spaceName = parser.space || "rgba";

							if ( values ) {
								parsed = inst[ spaceName ]( values );

								// If this was an rgba parse the assignment might happen twice
								// oh well....
								inst[ spaces[ spaceName ].cache ] = parsed[ spaces[ spaceName ].cache ];
								rgba = inst._rgba = parsed._rgba;

								// Exit each( stringParsers ) here because we matched
								return false;
							}
						} );

						// Found a stringParser that handled it
						if ( rgba.length ) {

							// If this came from a parsed string, force "transparent" when alpha is 0
							// chrome, (and maybe others) return "transparent" as rgba(0,0,0,0)
							if ( rgba.join() === "0,0,0,0" ) {
								jQuery.extend( rgba, colors.transparent );
							}
							return inst;
						}

						// Named colors
						return colors[ string ];
					}

					color.fn = jQuery.extend( color.prototype, {
						parse: function( red, green, blue, alpha ) {
							if ( red === undefined ) {
								this._rgba = [ null, null, null, null ];
								return this;
							}
							if ( red.jquery || red.nodeType ) {
								red = jQuery( red ).css( green );
								green = undefined;
							}

							var inst = this,
								type = jQuery.type( red ),
								rgba = this._rgba = [];

							// More than 1 argument specified - assume ( red, green, blue, alpha )
							if ( green !== undefined ) {
								red = [ red, green, blue, alpha ];
								type = "array";
							}

							if ( type === "string" ) {
								return this.parse( stringParse( red ) || colors._default );
							}

							if ( type === "array" ) {
								each( spaces.rgba.props, function( key, prop ) {
									rgba[ prop.idx ] = clamp( red[ prop.idx ], prop );
								} );
								return this;
							}

							if ( type === "object" ) {
								if ( red instanceof color ) {
									each( spaces, function( spaceName, space ) {
										if ( red[ space.cache ] ) {
											inst[ space.cache ] = red[ space.cache ].slice();
										}
									} );
								} else {
									each( spaces, function( spaceName, space ) {
										var cache = space.cache;
										each( space.props, function( key, prop ) {

											// If the cache doesn't exist, and we know how to convert
											if ( !inst[ cache ] && space.to ) {

												// If the value was null, we don't need to copy it
												// if the key was alpha, we don't need to copy it either
												if ( key === "alpha" || red[ key ] == null ) {
													return;
												}
												inst[ cache ] = space.to( inst._rgba );
											}

											// This is the only case where we allow nulls for ALL properties.
											// call clamp with alwaysAllowEmpty
											inst[ cache ][ prop.idx ] = clamp( red[ key ], prop, true );
										} );

										// Everything defined but alpha?
										if ( inst[ cache ] &&
											jQuery.inArray( null, inst[ cache ].slice( 0, 3 ) ) < 0 ) {

											// Use the default of 1
											inst[ cache ][ 3 ] = 1;
											if ( space.from ) {
												inst._rgba = space.from( inst[ cache ] );
											}
										}
									} );
								}
								return this;
							}
						},
						is: function( compare ) {
							var is = color( compare ),
								same = true,
								inst = this;

							each( spaces, function( _, space ) {
								var localCache,
									isCache = is[ space.cache ];
								if ( isCache ) {
									localCache = inst[ space.cache ] || space.to && space.to( inst._rgba ) || [];
									each( space.props, function( _, prop ) {
										if ( isCache[ prop.idx ] != null ) {
											same = ( isCache[ prop.idx ] === localCache[ prop.idx ] );
											return same;
										}
									} );
								}
								return same;
							} );
							return same;
						},
						_space: function() {
							var used = [],
								inst = this;
							each( spaces, function( spaceName, space ) {
								if ( inst[ space.cache ] ) {
									used.push( spaceName );
								}
							} );
							return used.pop();
						},
						transition: function( other, distance ) {
							var end = color( other ),
								spaceName = end._space(),
								space = spaces[ spaceName ],
								startColor = this.alpha() === 0 ? color( "transparent" ) : this,
								start = startColor[ space.cache ] || space.to( startColor._rgba ),
								result = start.slice();

							end = end[ space.cache ];
							each( space.props, function( key, prop ) {
								var index = prop.idx,
									startValue = start[ index ],
									endValue = end[ index ],
									type = propTypes[ prop.type ] || {};

								// If null, don't override start value
								if ( endValue === null ) {
									return;
								}

								// If null - use end
								if ( startValue === null ) {
									result[ index ] = endValue;
								} else {
									if ( type.mod ) {
										if ( endValue - startValue > type.mod / 2 ) {
											startValue += type.mod;
										} else if ( startValue - endValue > type.mod / 2 ) {
											startValue -= type.mod;
										}
									}
									result[ index ] = clamp( ( endValue - startValue ) * distance + startValue, prop );
								}
							} );
							return this[ spaceName ]( result );
						},
						blend: function( opaque ) {

							// If we are already opaque - return ourself
							if ( this._rgba[ 3 ] === 1 ) {
								return this;
							}

							var rgb = this._rgba.slice(),
								a = rgb.pop(),
								blend = color( opaque )._rgba;

							return color( jQuery.map( rgb, function( v, i ) {
								return ( 1 - a ) * blend[ i ] + a * v;
							} ) );
						},
						toRgbaString: function() {
							var prefix = "rgba(",
								rgba = jQuery.map( this._rgba, function( v, i ) {
									return v == null ? ( i > 2 ? 1 : 0 ) : v;
								} );

							if ( rgba[ 3 ] === 1 ) {
								rgba.pop();
								prefix = "rgb(";
							}

							return prefix + rgba.join() + ")";
						},
						toHslaString: function() {
							var prefix = "hsla(",
								hsla = jQuery.map( this.hsla(), function( v, i ) {
									if ( v == null ) {
										v = i > 2 ? 1 : 0;
									}

									// Catch 1 and 2
									if ( i && i < 3 ) {
										v = Math.round( v * 100 ) + "%";
									}
									return v;
								} );

							if ( hsla[ 3 ] === 1 ) {
								hsla.pop();
								prefix = "hsl(";
							}
							return prefix + hsla.join() + ")";
						},
						toHexString: function( includeAlpha ) {
							var rgba = this._rgba.slice(),
								alpha = rgba.pop();

							if ( includeAlpha ) {
								rgba.push( ~~( alpha * 255 ) );
							}

							return "#" + jQuery.map( rgba, function( v ) {

								// Default to 0 when nulls exist
								v = ( v || 0 ).toString( 16 );
								return v.length === 1 ? "0" + v : v;
							} ).join( "" );
						},
						toString: function() {
							return this._rgba[ 3 ] === 0 ? "transparent" : this.toRgbaString();
						}
					} );
					color.fn.parse.prototype = color.fn;

// Hsla conversions adapted from:
// https://code.google.com/p/maashaack/source/browse/packages/graphics/trunk/src/graphics/colors/HUE2RGB.as?r=5021

					function hue2rgb( p, q, h ) {
						h = ( h + 1 ) % 1;
						if ( h * 6 < 1 ) {
							return p + ( q - p ) * h * 6;
						}
						if ( h * 2 < 1 ) {
							return q;
						}
						if ( h * 3 < 2 ) {
							return p + ( q - p ) * ( ( 2 / 3 ) - h ) * 6;
						}
						return p;
					}

					spaces.hsla.to = function( rgba ) {
						if ( rgba[ 0 ] == null || rgba[ 1 ] == null || rgba[ 2 ] == null ) {
							return [ null, null, null, rgba[ 3 ] ];
						}
						var r = rgba[ 0 ] / 255,
							g = rgba[ 1 ] / 255,
							b = rgba[ 2 ] / 255,
							a = rgba[ 3 ],
							max = Math.max( r, g, b ),
							min = Math.min( r, g, b ),
							diff = max - min,
							add = max + min,
							l = add * 0.5,
							h, s;

						if ( min === max ) {
							h = 0;
						} else if ( r === max ) {
							h = ( 60 * ( g - b ) / diff ) + 360;
						} else if ( g === max ) {
							h = ( 60 * ( b - r ) / diff ) + 120;
						} else {
							h = ( 60 * ( r - g ) / diff ) + 240;
						}

						// Chroma (diff) == 0 means greyscale which, by definition, saturation = 0%
						// otherwise, saturation is based on the ratio of chroma (diff) to lightness (add)
						if ( diff === 0 ) {
							s = 0;
						} else if ( l <= 0.5 ) {
							s = diff / add;
						} else {
							s = diff / ( 2 - add );
						}
						return [ Math.round( h ) % 360, s, l, a == null ? 1 : a ];
					};

					spaces.hsla.from = function( hsla ) {
						if ( hsla[ 0 ] == null || hsla[ 1 ] == null || hsla[ 2 ] == null ) {
							return [ null, null, null, hsla[ 3 ] ];
						}
						var h = hsla[ 0 ] / 360,
							s = hsla[ 1 ],
							l = hsla[ 2 ],
							a = hsla[ 3 ],
							q = l <= 0.5 ? l * ( 1 + s ) : l + s - l * s,
							p = 2 * l - q;

						return [
							Math.round( hue2rgb( p, q, h + ( 1 / 3 ) ) * 255 ),
							Math.round( hue2rgb( p, q, h ) * 255 ),
							Math.round( hue2rgb( p, q, h - ( 1 / 3 ) ) * 255 ),
							a
						];
					};

					each( spaces, function( spaceName, space ) {
						var props = space.props,
							cache = space.cache,
							to = space.to,
							from = space.from;

						// Makes rgba() and hsla()
						color.fn[ spaceName ] = function( value ) {

							// Generate a cache for this space if it doesn't exist
							if ( to && !this[ cache ] ) {
								this[ cache ] = to( this._rgba );
							}
							if ( value === undefined ) {
								return this[ cache ].slice();
							}

							var ret,
								type = jQuery.type( value ),
								arr = ( type === "array" || type === "object" ) ? value : arguments,
								local = this[ cache ].slice();

							each( props, function( key, prop ) {
								var val = arr[ type === "object" ? key : prop.idx ];
								if ( val == null ) {
									val = local[ prop.idx ];
								}
								local[ prop.idx ] = clamp( val, prop );
							} );

							if ( from ) {
								ret = color( from( local ) );
								ret[ cache ] = local;
								return ret;
							} else {
								return color( local );
							}
						};

						// Makes red() green() blue() alpha() hue() saturation() lightness()
						each( props, function( key, prop ) {

							// Alpha is included in more than one space
							if ( color.fn[ key ] ) {
								return;
							}
							color.fn[ key ] = function( value ) {
								var vtype = jQuery.type( value ),
									fn = ( key === "alpha" ? ( this._hsla ? "hsla" : "rgba" ) : spaceName ),
									local = this[ fn ](),
									cur = local[ prop.idx ],
									match;

								if ( vtype === "undefined" ) {
									return cur;
								}

								if ( vtype === "function" ) {
									value = value.call( this, cur );
									vtype = jQuery.type( value );
								}
								if ( value == null && prop.empty ) {
									return this;
								}
								if ( vtype === "string" ) {
									match = rplusequals.exec( value );
									if ( match ) {
										value = cur + parseFloat( match[ 2 ] ) * ( match[ 1 ] === "+" ? 1 : -1 );
									}
								}
								local[ prop.idx ] = value;
								return this[ fn ]( local );
							};
						} );
					} );

// Add cssHook and .fx.step function for each named hook.
// accept a space separated string of properties
					color.hook = function( hook ) {
						var hooks = hook.split( " " );
						each( hooks, function( i, hook ) {
							jQuery.cssHooks[ hook ] = {
								set: function( elem, value ) {
									var parsed, curElem,
										backgroundColor = "";

									if ( value !== "transparent" && ( jQuery.type( value ) !== "string" ||
										( parsed = stringParse( value ) ) ) ) {
										value = color( parsed || value );
										if ( !support.rgba && value._rgba[ 3 ] !== 1 ) {
											curElem = hook === "backgroundColor" ? elem.parentNode : elem;
											while (
												( backgroundColor === "" || backgroundColor === "transparent" ) &&
												curElem && curElem.style
												) {
												try {
													backgroundColor = jQuery.css( curElem, "backgroundColor" );
													curElem = curElem.parentNode;
												} catch ( e ) {
												}
											}

											value = value.blend( backgroundColor && backgroundColor !== "transparent" ?
												backgroundColor :
												"_default" );
										}

										value = value.toRgbaString();
									}
									try {
										elem.style[ hook ] = value;
									} catch ( e ) {

										// Wrapped to prevent IE from throwing errors on "invalid" values like
										// 'auto' or 'inherit'
									}
								}
							};
							jQuery.fx.step[ hook ] = function( fx ) {
								if ( !fx.colorInit ) {
									fx.start = color( fx.elem, hook );
									fx.end = color( fx.end );
									fx.colorInit = true;
								}
								jQuery.cssHooks[ hook ].set( fx.elem, fx.start.transition( fx.end, fx.pos ) );
							};
						} );

					};

					color.hook( stepHooks );

					jQuery.cssHooks.borderColor = {
						expand: function( value ) {
							var expanded = {};

							each( [ "Top", "Right", "Bottom", "Left" ], function( i, part ) {
								expanded[ "border" + part + "Color" ] = value;
							} );
							return expanded;
						}
					};

// Basic color names only.
// Usage of any of the other color names requires adding yourself or including
// jquery.color.svg-names.js.
					colors = jQuery.Color.names = {

						// 4.1. Basic color keywords
						aqua: "#00ffff",
						black: "#000000",
						blue: "#0000ff",
						fuchsia: "#ff00ff",
						gray: "#808080",
						green: "#008000",
						lime: "#00ff00",
						maroon: "#800000",
						navy: "#000080",
						olive: "#808000",
						purple: "#800080",
						red: "#ff0000",
						silver: "#c0c0c0",
						teal: "#008080",
						white: "#ffffff",
						yellow: "#ffff00",

						// 4.2.3. "transparent" color keyword
						transparent: [ null, null, null, 0 ],

						_default: "#ffffff"
					};

				} )( jQuery );

				/******************************************************************************/
				/****************************** CLASS ANIMATIONS ******************************/
				/******************************************************************************/
				( function() {

					var classAnimationActions = [ "add", "remove", "toggle" ],
						shorthandStyles = {
							border: 1,
							borderBottom: 1,
							borderColor: 1,
							borderLeft: 1,
							borderRight: 1,
							borderTop: 1,
							borderWidth: 1,
							margin: 1,
							padding: 1
						};

					$.each(
						[ "borderLeftStyle", "borderRightStyle", "borderBottomStyle", "borderTopStyle" ],
						function( _, prop ) {
							$.fx.step[ prop ] = function( fx ) {
								if ( fx.end !== "none" && !fx.setAttr || fx.pos === 1 && !fx.setAttr ) {
									jQuery.style( fx.elem, prop, fx.end );
									fx.setAttr = true;
								}
							};
						}
					);

					function getElementStyles( elem ) {
						var key, len,
							style = elem.ownerDocument.defaultView ?
								elem.ownerDocument.defaultView.getComputedStyle( elem, null ) :
								elem.currentStyle,
							styles = {};

						if ( style && style.length && style[ 0 ] && style[ style[ 0 ] ] ) {
							len = style.length;
							while ( len-- ) {
								key = style[ len ];
								if ( typeof style[ key ] === "string" ) {
									styles[ $.camelCase( key ) ] = style[ key ];
								}
							}

							// Support: Opera, IE <9
						} else {
							for ( key in style ) {
								if ( typeof style[ key ] === "string" ) {
									styles[ key ] = style[ key ];
								}
							}
						}

						return styles;
					}

					function styleDifference( oldStyle, newStyle ) {
						var diff = {},
							name, value;

						for ( name in newStyle ) {
							value = newStyle[ name ];
							if ( oldStyle[ name ] !== value ) {
								if ( !shorthandStyles[ name ] ) {
									if ( $.fx.step[ name ] || !isNaN( parseFloat( value ) ) ) {
										diff[ name ] = value;
									}
								}
							}
						}

						return diff;
					}

// Support: jQuery <1.8
					if ( !$.fn.addBack ) {
						$.fn.addBack = function( selector ) {
							return this.add( selector == null ?
								this.prevObject : this.prevObject.filter( selector )
							);
						};
					}

					$.effects.animateClass = function( value, duration, easing, callback ) {
						var o = $.speed( duration, easing, callback );

						return this.queue( function() {
							var animated = $( this ),
								baseClass = animated.attr( "class" ) || "",
								applyClassChange,
								allAnimations = o.children ? animated.find( "*" ).addBack() : animated;

							// Map the animated objects to store the original styles.
							allAnimations = allAnimations.map( function() {
								var el = $( this );
								return {
									el: el,
									start: getElementStyles( this )
								};
							} );

							// Apply class change
							applyClassChange = function() {
								$.each( classAnimationActions, function( i, action ) {
									if ( value[ action ] ) {
										animated[ action + "Class" ]( value[ action ] );
									}
								} );
							};
							applyClassChange();

							// Map all animated objects again - calculate new styles and diff
							allAnimations = allAnimations.map( function() {
								this.end = getElementStyles( this.el[ 0 ] );
								this.diff = styleDifference( this.start, this.end );
								return this;
							} );

							// Apply original class
							animated.attr( "class", baseClass );

							// Map all animated objects again - this time collecting a promise
							allAnimations = allAnimations.map( function() {
								var styleInfo = this,
									dfd = $.Deferred(),
									opts = $.extend( {}, o, {
										queue: false,
										complete: function() {
											dfd.resolve( styleInfo );
										}
									} );

								this.el.animate( this.diff, opts );
								return dfd.promise();
							} );

							// Once all animations have completed:
							$.when.apply( $, allAnimations.get() ).done( function() {

								// Set the final class
								applyClassChange();

								// For each animated element,
								// clear all css properties that were animated
								$.each( arguments, function() {
									var el = this.el;
									$.each( this.diff, function( key ) {
										el.css( key, "" );
									} );
								} );

								// This is guarnteed to be there if you use jQuery.speed()
								// it also handles dequeuing the next anim...
								o.complete.call( animated[ 0 ] );
							} );
						} );
					};

					$.fn.extend( {
						addClass: ( function( orig ) {
							return function( classNames, speed, easing, callback ) {
								return speed ?
									$.effects.animateClass.call( this,
										{ add: classNames }, speed, easing, callback ) :
									orig.apply( this, arguments );
							};
						} )( $.fn.addClass ),

						removeClass: ( function( orig ) {
							return function( classNames, speed, easing, callback ) {
								return arguments.length > 1 ?
									$.effects.animateClass.call( this,
										{ remove: classNames }, speed, easing, callback ) :
									orig.apply( this, arguments );
							};
						} )( $.fn.removeClass ),

						toggleClass: ( function( orig ) {
							return function( classNames, force, speed, easing, callback ) {
								if ( typeof force === "boolean" || force === undefined ) {
									if ( !speed ) {

										// Without speed parameter
										return orig.apply( this, arguments );
									} else {
										return $.effects.animateClass.call( this,
											( force ? { add: classNames } : { remove: classNames } ),
											speed, easing, callback );
									}
								} else {

									// Without force parameter
									return $.effects.animateClass.call( this,
										{ toggle: classNames }, force, speed, easing );
								}
							};
						} )( $.fn.toggleClass ),

						switchClass: function( remove, add, speed, easing, callback ) {
							return $.effects.animateClass.call( this, {
								add: add,
								remove: remove
							}, speed, easing, callback );
						}
					} );

				} )();

				/******************************************************************************/
				/*********************************** EFFECTS **********************************/
				/******************************************************************************/

				( function() {

					if ( $.expr && $.expr.filters && $.expr.filters.animated ) {
						$.expr.filters.animated = ( function( orig ) {
							return function( elem ) {
								return !!$( elem ).data( dataSpaceAnimated ) || orig( elem );
							};
						} )( $.expr.filters.animated );
					}

					if ( $.uiBackCompat !== false ) {
						$.extend( $.effects, {

							// Saves a set of properties in a data storage
							save: function( element, set ) {
								var i = 0, length = set.length;
								for ( ; i < length; i++ ) {
									if ( set[ i ] !== null ) {
										element.data( dataSpace + set[ i ], element[ 0 ].style[ set[ i ] ] );
									}
								}
							},

							// Restores a set of previously saved properties from a data storage
							restore: function( element, set ) {
								var val, i = 0, length = set.length;
								for ( ; i < length; i++ ) {
									if ( set[ i ] !== null ) {
										val = element.data( dataSpace + set[ i ] );
										element.css( set[ i ], val );
									}
								}
							},

							setMode: function( el, mode ) {
								if ( mode === "toggle" ) {
									mode = el.is( ":hidden" ) ? "show" : "hide";
								}
								return mode;
							},

							// Wraps the element around a wrapper that copies position properties
							createWrapper: function( element ) {

								// If the element is already wrapped, return it
								if ( element.parent().is( ".ui-effects-wrapper" ) ) {
									return element.parent();
								}

								// Wrap the element
								var props = {
										width: element.outerWidth( true ),
										height: element.outerHeight( true ),
										"float": element.css( "float" )
									},
									wrapper = $( "<div></div>" )
										.addClass( "ui-effects-wrapper" )
										.css( {
											fontSize: "100%",
											background: "transparent",
											border: "none",
											margin: 0,
											padding: 0
										} ),

									// Store the size in case width/height are defined in % - Fixes #5245
									size = {
										width: element.width(),
										height: element.height()
									},
									active = document.activeElement;

								// Support: Firefox
								// Firefox incorrectly exposes anonymous content
								// https://bugzilla.mozilla.org/show_bug.cgi?id=561664
								try {
									active.id;
								} catch ( e ) {
									active = document.body;
								}

								element.wrap( wrapper );

								// Fixes #7595 - Elements lose focus when wrapped.
								if ( element[ 0 ] === active || $.contains( element[ 0 ], active ) ) {
									$( active ).trigger( "focus" );
								}

								// Hotfix for jQuery 1.4 since some change in wrap() seems to actually
								// lose the reference to the wrapped element
								wrapper = element.parent();

								// Transfer positioning properties to the wrapper
								if ( element.css( "position" ) === "static" ) {
									wrapper.css( { position: "relative" } );
									element.css( { position: "relative" } );
								} else {
									$.extend( props, {
										position: element.css( "position" ),
										zIndex: element.css( "z-index" )
									} );
									$.each( [ "top", "left", "bottom", "right" ], function( i, pos ) {
										props[ pos ] = element.css( pos );
										if ( isNaN( parseInt( props[ pos ], 10 ) ) ) {
											props[ pos ] = "auto";
										}
									} );
									element.css( {
										position: "relative",
										top: 0,
										left: 0,
										right: "auto",
										bottom: "auto"
									} );
								}
								element.css( size );

								return wrapper.css( props ).show();
							},

							removeWrapper: function( element ) {
								var active = document.activeElement;

								if ( element.parent().is( ".ui-effects-wrapper" ) ) {
									element.parent().replaceWith( element );

									// Fixes #7595 - Elements lose focus when wrapped.
									if ( element[ 0 ] === active || $.contains( element[ 0 ], active ) ) {
										$( active ).trigger( "focus" );
									}
								}

								return element;
							}
						} );
					}

					$.extend( $.effects, {
						version: "1.12.1",

						define: function( name, mode, effect ) {
							if ( !effect ) {
								effect = mode;
								mode = "effect";
							}

							$.effects.effect[ name ] = effect;
							$.effects.effect[ name ].mode = mode;

							return effect;
						},

						scaledDimensions: function( element, percent, direction ) {
							if ( percent === 0 ) {
								return {
									height: 0,
									width: 0,
									outerHeight: 0,
									outerWidth: 0
								};
							}

							var x = direction !== "horizontal" ? ( ( percent || 100 ) / 100 ) : 1,
								y = direction !== "vertical" ? ( ( percent || 100 ) / 100 ) : 1;

							return {
								height: element.height() * y,
								width: element.width() * x,
								outerHeight: element.outerHeight() * y,
								outerWidth: element.outerWidth() * x
							};

						},

						clipToBox: function( animation ) {
							return {
								width: animation.clip.right - animation.clip.left,
								height: animation.clip.bottom - animation.clip.top,
								left: animation.clip.left,
								top: animation.clip.top
							};
						},

						// Injects recently queued functions to be first in line (after "inprogress")
						unshift: function( element, queueLength, count ) {
							var queue = element.queue();

							if ( queueLength > 1 ) {
								queue.splice.apply( queue,
									[ 1, 0 ].concat( queue.splice( queueLength, count ) ) );
							}
							element.dequeue();
						},

						saveStyle: function( element ) {
							element.data( dataSpaceStyle, element[ 0 ].style.cssText );
						},

						restoreStyle: function( element ) {
							element[ 0 ].style.cssText = element.data( dataSpaceStyle ) || "";
							element.removeData( dataSpaceStyle );
						},

						mode: function( element, mode ) {
							var hidden = element.is( ":hidden" );

							if ( mode === "toggle" ) {
								mode = hidden ? "show" : "hide";
							}
							if ( hidden ? mode === "hide" : mode === "show" ) {
								mode = "none";
							}
							return mode;
						},

						// Translates a [top,left] array into a baseline value
						getBaseline: function( origin, original ) {
							var y, x;

							switch ( origin[ 0 ] ) {
								case "top":
									y = 0;
									break;
								case "middle":
									y = 0.5;
									break;
								case "bottom":
									y = 1;
									break;
								default:
									y = origin[ 0 ] / original.height;
							}

							switch ( origin[ 1 ] ) {
								case "left":
									x = 0;
									break;
								case "center":
									x = 0.5;
									break;
								case "right":
									x = 1;
									break;
								default:
									x = origin[ 1 ] / original.width;
							}

							return {
								x: x,
								y: y
							};
						},

						// Creates a placeholder element so that the original element can be made absolute
						createPlaceholder: function( element ) {
							var placeholder,
								cssPosition = element.css( "position" ),
								position = element.position();

							// Lock in margins first to account for form elements, which
							// will change margin if you explicitly set height
							// see: http://jsfiddle.net/JZSMt/3/ https://bugs.webkit.org/show_bug.cgi?id=107380
							// Support: Safari
							element.css( {
								marginTop: element.css( "marginTop" ),
								marginBottom: element.css( "marginBottom" ),
								marginLeft: element.css( "marginLeft" ),
								marginRight: element.css( "marginRight" )
							} )
								.outerWidth( element.outerWidth() )
								.outerHeight( element.outerHeight() );

							if ( /^(static|relative)/.test( cssPosition ) ) {
								cssPosition = "absolute";

								placeholder = $( "<" + element[ 0 ].nodeName + ">" ).insertAfter( element ).css( {

									// Convert inline to inline block to account for inline elements
									// that turn to inline block based on content (like img)
									display: /^(inline|ruby)/.test( element.css( "display" ) ) ?
										"inline-block" :
										"block",
									visibility: "hidden",

									// Margins need to be set to account for margin collapse
									marginTop: element.css( "marginTop" ),
									marginBottom: element.css( "marginBottom" ),
									marginLeft: element.css( "marginLeft" ),
									marginRight: element.css( "marginRight" ),
									"float": element.css( "float" )
								} )
									.outerWidth( element.outerWidth() )
									.outerHeight( element.outerHeight() )
									.addClass( "ui-effects-placeholder" );

								element.data( dataSpace + "placeholder", placeholder );
							}

							element.css( {
								position: cssPosition,
								left: position.left,
								top: position.top
							} );

							return placeholder;
						},

						removePlaceholder: function( element ) {
							var dataKey = dataSpace + "placeholder",
								placeholder = element.data( dataKey );

							if ( placeholder ) {
								placeholder.remove();
								element.removeData( dataKey );
							}
						},

						// Removes a placeholder if it exists and restores
						// properties that were modified during placeholder creation
						cleanUp: function( element ) {
							$.effects.restoreStyle( element );
							$.effects.removePlaceholder( element );
						},

						setTransition: function( element, list, factor, value ) {
							value = value || {};
							$.each( list, function( i, x ) {
								var unit = element.cssUnit( x );
								if ( unit[ 0 ] > 0 ) {
									value[ x ] = unit[ 0 ] * factor + unit[ 1 ];
								}
							} );
							return value;
						}
					} );

// Return an effect options object for the given parameters:
					function _normalizeArguments( effect, options, speed, callback ) {

						// Allow passing all options as the first parameter
						if ( $.isPlainObject( effect ) ) {
							options = effect;
							effect = effect.effect;
						}

						// Convert to an object
						effect = { effect: effect };

						// Catch (effect, null, ...)
						if ( options == null ) {
							options = {};
						}

						// Catch (effect, callback)
						if ( $.isFunction( options ) ) {
							callback = options;
							speed = null;
							options = {};
						}

						// Catch (effect, speed, ?)
						if ( typeof options === "number" || $.fx.speeds[ options ] ) {
							callback = speed;
							speed = options;
							options = {};
						}

						// Catch (effect, options, callback)
						if ( $.isFunction( speed ) ) {
							callback = speed;
							speed = null;
						}

						// Add options to effect
						if ( options ) {
							$.extend( effect, options );
						}

						speed = speed || options.duration;
						effect.duration = $.fx.off ? 0 :
							typeof speed === "number" ? speed :
								speed in $.fx.speeds ? $.fx.speeds[ speed ] :
									$.fx.speeds._default;

						effect.complete = callback || options.complete;

						return effect;
					}

					function standardAnimationOption( option ) {

						// Valid standard speeds (nothing, number, named speed)
						if ( !option || typeof option === "number" || $.fx.speeds[ option ] ) {
							return true;
						}

						// Invalid strings - treat as "normal" speed
						if ( typeof option === "string" && !$.effects.effect[ option ] ) {
							return true;
						}

						// Complete callback
						if ( $.isFunction( option ) ) {
							return true;
						}

						// Options hash (but not naming an effect)
						if ( typeof option === "object" && !option.effect ) {
							return true;
						}

						// Didn't match any standard API
						return false;
					}

					$.fn.extend( {
						effect: function( /* effect, options, speed, callback */ ) {
							var args = _normalizeArguments.apply( this, arguments ),
								effectMethod = $.effects.effect[ args.effect ],
								defaultMode = effectMethod.mode,
								queue = args.queue,
								queueName = queue || "fx",
								complete = args.complete,
								mode = args.mode,
								modes = [],
								prefilter = function( next ) {
									var el = $( this ),
										normalizedMode = $.effects.mode( el, mode ) || defaultMode;

									// Sentinel for duck-punching the :animated psuedo-selector
									el.data( dataSpaceAnimated, true );

									// Save effect mode for later use,
									// we can't just call $.effects.mode again later,
									// as the .show() below destroys the initial state
									modes.push( normalizedMode );

									// See $.uiBackCompat inside of run() for removal of defaultMode in 1.13
									if ( defaultMode && ( normalizedMode === "show" ||
										( normalizedMode === defaultMode && normalizedMode === "hide" ) ) ) {
										el.show();
									}

									if ( !defaultMode || normalizedMode !== "none" ) {
										$.effects.saveStyle( el );
									}

									if ( $.isFunction( next ) ) {
										next();
									}
								};

							if ( $.fx.off || !effectMethod ) {

								// Delegate to the original method (e.g., .show()) if possible
								if ( mode ) {
									return this[ mode ]( args.duration, complete );
								} else {
									return this.each( function() {
										if ( complete ) {
											complete.call( this );
										}
									} );
								}
							}

							function run( next ) {
								var elem = $( this );

								function cleanup() {
									elem.removeData( dataSpaceAnimated );

									$.effects.cleanUp( elem );

									if ( args.mode === "hide" ) {
										elem.hide();
									}

									done();
								}

								function done() {
									if ( $.isFunction( complete ) ) {
										complete.call( elem[ 0 ] );
									}

									if ( $.isFunction( next ) ) {
										next();
									}
								}

								// Override mode option on a per element basis,
								// as toggle can be either show or hide depending on element state
								args.mode = modes.shift();

								if ( $.uiBackCompat !== false && !defaultMode ) {
									if ( elem.is( ":hidden" ) ? mode === "hide" : mode === "show" ) {

										// Call the core method to track "olddisplay" properly
										elem[ mode ]();
										done();
									} else {
										effectMethod.call( elem[ 0 ], args, done );
									}
								} else {
									if ( args.mode === "none" ) {

										// Call the core method to track "olddisplay" properly
										elem[ mode ]();
										done();
									} else {
										effectMethod.call( elem[ 0 ], args, cleanup );
									}
								}
							}

							// Run prefilter on all elements first to ensure that
							// any showing or hiding happens before placeholder creation,
							// which ensures that any layout changes are correctly captured.
							return queue === false ?
								this.each( prefilter ).each( run ) :
								this.queue( queueName, prefilter ).queue( queueName, run );
						},

						show: ( function( orig ) {
							return function( option ) {
								if ( standardAnimationOption( option ) ) {
									return orig.apply( this, arguments );
								} else {
									var args = _normalizeArguments.apply( this, arguments );
									args.mode = "show";
									return this.effect.call( this, args );
								}
							};
						} )( $.fn.show ),

						hide: ( function( orig ) {
							return function( option ) {
								if ( standardAnimationOption( option ) ) {
									return orig.apply( this, arguments );
								} else {
									var args = _normalizeArguments.apply( this, arguments );
									args.mode = "hide";
									return this.effect.call( this, args );
								}
							};
						} )( $.fn.hide ),

						toggle: ( function( orig ) {
							return function( option ) {
								if ( standardAnimationOption( option ) || typeof option === "boolean" ) {
									return orig.apply( this, arguments );
								} else {
									var args = _normalizeArguments.apply( this, arguments );
									args.mode = "toggle";
									return this.effect.call( this, args );
								}
							};
						} )( $.fn.toggle ),

						cssUnit: function( key ) {
							var style = this.css( key ),
								val = [];

							$.each( [ "em", "px", "%", "pt" ], function( i, unit ) {
								if ( style.indexOf( unit ) > 0 ) {
									val = [ parseFloat( style ), unit ];
								}
							} );
							return val;
						},

						cssClip: function( clipObj ) {
							if ( clipObj ) {
								return this.css( "clip", "rect(" + clipObj.top + "px " + clipObj.right + "px " +
									clipObj.bottom + "px " + clipObj.left + "px)" );
							}
							return parseClip( this.css( "clip" ), this );
						},

						transfer: function( options, done ) {
							var element = $( this ),
								target = $( options.to ),
								targetFixed = target.css( "position" ) === "fixed",
								body = $( "body" ),
								fixTop = targetFixed ? body.scrollTop() : 0,
								fixLeft = targetFixed ? body.scrollLeft() : 0,
								endPosition = target.offset(),
								animation = {
									top: endPosition.top - fixTop,
									left: endPosition.left - fixLeft,
									height: target.innerHeight(),
									width: target.innerWidth()
								},
								startPosition = element.offset(),
								transfer = $( "<div class='ui-effects-transfer'></div>" )
									.appendTo( "body" )
									.addClass( options.className )
									.css( {
										top: startPosition.top - fixTop,
										left: startPosition.left - fixLeft,
										height: element.innerHeight(),
										width: element.innerWidth(),
										position: targetFixed ? "fixed" : "absolute"
									} )
									.animate( animation, options.duration, options.easing, function() {
										transfer.remove();
										if ( $.isFunction( done ) ) {
											done();
										}
									} );
						}
					} );

					function parseClip( str, element ) {
						var outerWidth = element.outerWidth(),
							outerHeight = element.outerHeight(),
							clipRegex = /^rect\((-?\d*\.?\d*px|-?\d+%|auto),?\s*(-?\d*\.?\d*px|-?\d+%|auto),?\s*(-?\d*\.?\d*px|-?\d+%|auto),?\s*(-?\d*\.?\d*px|-?\d+%|auto)\)$/,
							values = clipRegex.exec( str ) || [ "", 0, outerWidth, outerHeight, 0 ];

						return {
							top: parseFloat( values[ 1 ] ) || 0,
							right: values[ 2 ] === "auto" ? outerWidth : parseFloat( values[ 2 ] ),
							bottom: values[ 3 ] === "auto" ? outerHeight : parseFloat( values[ 3 ] ),
							left: parseFloat( values[ 4 ] ) || 0
						};
					}

					$.fx.step.clip = function( fx ) {
						if ( !fx.clipInit ) {
							fx.start = $( fx.elem ).cssClip();
							if ( typeof fx.end === "string" ) {
								fx.end = parseClip( fx.end, fx.elem );
							}
							fx.clipInit = true;
						}

						$( fx.elem ).cssClip( {
							top: fx.pos * ( fx.end.top - fx.start.top ) + fx.start.top,
							right: fx.pos * ( fx.end.right - fx.start.right ) + fx.start.right,
							bottom: fx.pos * ( fx.end.bottom - fx.start.bottom ) + fx.start.bottom,
							left: fx.pos * ( fx.end.left - fx.start.left ) + fx.start.left
						} );
					};

				} )();

				/******************************************************************************/
				/*********************************** EASING ***********************************/
				/******************************************************************************/

				( function() {

// Based on easing equations from Robert Penner (http://www.robertpenner.com/easing)

					var baseEasings = {};

					$.each( [ "Quad", "Cubic", "Quart", "Quint", "Expo" ], function( i, name ) {
						baseEasings[ name ] = function( p ) {
							return Math.pow( p, i + 2 );
						};
					} );

					$.extend( baseEasings, {
						Sine: function( p ) {
							return 1 - Math.cos( p * Math.PI / 2 );
						},
						Circ: function( p ) {
							return 1 - Math.sqrt( 1 - p * p );
						},
						Elastic: function( p ) {
							return p === 0 || p === 1 ? p :
								-Math.pow( 2, 8 * ( p - 1 ) ) * Math.sin( ( ( p - 1 ) * 80 - 7.5 ) * Math.PI / 15 );
						},
						Back: function( p ) {
							return p * p * ( 3 * p - 2 );
						},
						Bounce: function( p ) {
							var pow2,
								bounce = 4;

							while ( p < ( ( pow2 = Math.pow( 2, --bounce ) ) - 1 ) / 11 ) {}
							return 1 / Math.pow( 4, 3 - bounce ) - 7.5625 * Math.pow( ( pow2 * 3 - 2 ) / 22 - p, 2 );
						}
					} );

					$.each( baseEasings, function( name, easeIn ) {
						$.easing[ "easeIn" + name ] = easeIn;
						$.easing[ "easeOut" + name ] = function( p ) {
							return 1 - easeIn( 1 - p );
						};
						$.easing[ "easeInOut" + name ] = function( p ) {
							return p < 0.5 ?
								easeIn( p * 2 ) / 2 :
								1 - easeIn( p * -2 + 2 ) / 2;
						};
					} );

				} )();

				return $.effects;

			} ) );


			/***/ }),

		/***/ "../node_modules/jquery-ui/ui/keycode.js":
		/*!***********************************************!*\
  !*** ../node_modules/jquery-ui/ui/keycode.js ***!
  \***********************************************/
		/*! no static exports found */
		/***/ (function(module, exports, __webpack_require__) {

			var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;/*!
 * jQuery UI Keycode 1.12.1
 * http://jqueryui.com
 *
 * Copyright jQuery Foundation and other contributors
 * Released under the MIT license.
 * http://jquery.org/license
 */

//>>label: Keycode
//>>group: Core
//>>description: Provide keycodes as keynames
//>>docs: http://api.jqueryui.com/jQuery.ui.keyCode/

			( function( factory ) {
				if ( true ) {

					// AMD. Register as an anonymous module.
					!(__WEBPACK_AMD_DEFINE_ARRAY__ = [ __webpack_require__(/*! jquery */ "jquery"), __webpack_require__(/*! ./version */ "../node_modules/jquery-ui/ui/version.js") ], __WEBPACK_AMD_DEFINE_FACTORY__ = (factory),
						__WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
							(__WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)) : __WEBPACK_AMD_DEFINE_FACTORY__),
					__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
				} else {}
			} ( function( $ ) {
				return $.ui.keyCode = {
					BACKSPACE: 8,
					COMMA: 188,
					DELETE: 46,
					DOWN: 40,
					END: 35,
					ENTER: 13,
					ESCAPE: 27,
					HOME: 36,
					LEFT: 37,
					PAGE_DOWN: 34,
					PAGE_UP: 33,
					PERIOD: 190,
					RIGHT: 39,
					SPACE: 32,
					TAB: 9,
					UP: 38
				};

			} ) );


			/***/ }),

		/***/ "../node_modules/jquery-ui/ui/version.js":
		/*!***********************************************!*\
  !*** ../node_modules/jquery-ui/ui/version.js ***!
  \***********************************************/
		/*! no static exports found */
		/***/ (function(module, exports, __webpack_require__) {

			var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;( function( factory ) {
				if ( true ) {

					// AMD. Register as an anonymous module.
					!(__WEBPACK_AMD_DEFINE_ARRAY__ = [ __webpack_require__(/*! jquery */ "jquery") ], __WEBPACK_AMD_DEFINE_FACTORY__ = (factory),
						__WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
							(__WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)) : __WEBPACK_AMD_DEFINE_FACTORY__),
					__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
				} else {}
			} ( function( $ ) {

				$.ui = $.ui || {};

				return $.ui.version = "1.12.1";

			} ) );


			/***/ }),

		/***/ "../node_modules/jquery-ui/ui/widgets/datepicker.js":
		/*!**********************************************************!*\
  !*** ../node_modules/jquery-ui/ui/widgets/datepicker.js ***!
  \**********************************************************/
		/*! no static exports found */
		/***/ (function(module, exports, __webpack_require__) {

			var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;// jscs:disable maximumLineLength
			/* jscs:disable requireCamelCaseOrUpperCaseIdentifiers */
			/*!
 * jQuery UI Datepicker 1.12.1
 * http://jqueryui.com
 *
 * Copyright jQuery Foundation and other contributors
 * Released under the MIT license.
 * http://jquery.org/license
 */

//>>label: Datepicker
//>>group: Widgets
//>>description: Displays a calendar from an input or inline for selecting dates.
//>>docs: http://api.jqueryui.com/datepicker/
//>>demos: http://jqueryui.com/datepicker/
//>>css.structure: ../../themes/base/core.css
//>>css.structure: ../../themes/base/datepicker.css
//>>css.theme: ../../themes/base/theme.css

			( function( factory ) {
				if ( true ) {

					// AMD. Register as an anonymous module.
					!(__WEBPACK_AMD_DEFINE_ARRAY__ = [
						__webpack_require__(/*! jquery */ "jquery"),
						__webpack_require__(/*! ../version */ "../node_modules/jquery-ui/ui/version.js"),
						__webpack_require__(/*! ../keycode */ "../node_modules/jquery-ui/ui/keycode.js")
					], __WEBPACK_AMD_DEFINE_FACTORY__ = (factory),
						__WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
							(__WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)) : __WEBPACK_AMD_DEFINE_FACTORY__),
					__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
				} else {}
			}( function( $ ) {

				$.extend( $.ui, { datepicker: { version: "1.12.1" } } );

				var datepicker_instActive;

				function datepicker_getZindex( elem ) {
					var position, value;
					while ( elem.length && elem[ 0 ] !== document ) {

						// Ignore z-index if position is set to a value where z-index is ignored by the browser
						// This makes behavior of this function consistent across browsers
						// WebKit always returns auto if the element is positioned
						position = elem.css( "position" );
						if ( position === "absolute" || position === "relative" || position === "fixed" ) {

							// IE returns 0 when zIndex is not specified
							// other browsers return a string
							// we ignore the case of nested elements with an explicit value of 0
							// <div style="z-index: -10;"><div style="z-index: 0;"></div></div>
							value = parseInt( elem.css( "zIndex" ), 10 );
							if ( !isNaN( value ) && value !== 0 ) {
								return value;
							}
						}
						elem = elem.parent();
					}

					return 0;
				}
				/* Date picker manager.
   Use the singleton instance of this class, $.datepicker, to interact with the date picker.
   Settings for (groups of) date pickers are maintained in an instance object,
   allowing multiple different settings on the same page. */

				function Datepicker() {
					this._curInst = null; // The current instance in use
					this._keyEvent = false; // If the last event was a key event
					this._disabledInputs = []; // List of date picker inputs that have been disabled
					this._datepickerShowing = false; // True if the popup picker is showing , false if not
					this._inDialog = false; // True if showing within a "dialog", false if not
					this._mainDivId = "ui-datepicker-div"; // The ID of the main datepicker division
					this._inlineClass = "ui-datepicker-inline"; // The name of the inline marker class
					this._appendClass = "ui-datepicker-append"; // The name of the append marker class
					this._triggerClass = "ui-datepicker-trigger"; // The name of the trigger marker class
					this._dialogClass = "ui-datepicker-dialog"; // The name of the dialog marker class
					this._disableClass = "ui-datepicker-disabled"; // The name of the disabled covering marker class
					this._unselectableClass = "ui-datepicker-unselectable"; // The name of the unselectable cell marker class
					this._currentClass = "ui-datepicker-current-day"; // The name of the current day marker class
					this._dayOverClass = "ui-datepicker-days-cell-over"; // The name of the day hover marker class
					this.regional = []; // Available regional settings, indexed by language code
					this.regional[ "" ] = { // Default regional settings
						closeText: "Done", // Display text for close link
						prevText: "Prev", // Display text for previous month link
						nextText: "Next", // Display text for next month link
						currentText: "Today", // Display text for current month link
						monthNames: [ "January","February","March","April","May","June",
							"July","August","September","October","November","December" ], // Names of months for drop-down and formatting
						monthNamesShort: [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ], // For formatting
						dayNames: [ "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" ], // For formatting
						dayNamesShort: [ "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" ], // For formatting
						dayNamesMin: [ "Su","Mo","Tu","We","Th","Fr","Sa" ], // Column headings for days starting at Sunday
						weekHeader: "Wk", // Column header for week of the year
						dateFormat: "mm/dd/yy", // See format options on parseDate
						firstDay: 0, // The first day of the week, Sun = 0, Mon = 1, ...
						isRTL: false, // True if right-to-left language, false if left-to-right
						showMonthAfterYear: false, // True if the year select precedes month, false for month then year
						yearSuffix: "" // Additional text to append to the year in the month headers
					};
					this._defaults = { // Global defaults for all the date picker instances
						showOn: "focus", // "focus" for popup on focus,
						// "button" for trigger button, or "both" for either
						showAnim: "fadeIn", // Name of jQuery animation for popup
						showOptions: {}, // Options for enhanced animations
						defaultDate: null, // Used when field is blank: actual date,
						// +/-number for offset from today, null for today
						appendText: "", // Display text following the input box, e.g. showing the format
						buttonText: "...", // Text for trigger button
						buttonImage: "", // URL for trigger button image
						buttonImageOnly: false, // True if the image appears alone, false if it appears on a button
						hideIfNoPrevNext: false, // True to hide next/previous month links
						// if not applicable, false to just disable them
						navigationAsDateFormat: false, // True if date formatting applied to prev/today/next links
						gotoCurrent: false, // True if today link goes back to current selection instead
						changeMonth: false, // True if month can be selected directly, false if only prev/next
						changeYear: false, // True if year can be selected directly, false if only prev/next
						yearRange: "c-10:c+10", // Range of years to display in drop-down,
						// either relative to today's year (-nn:+nn), relative to currently displayed year
						// (c-nn:c+nn), absolute (nnnn:nnnn), or a combination of the above (nnnn:-n)
						showOtherMonths: false, // True to show dates in other months, false to leave blank
						selectOtherMonths: false, // True to allow selection of dates in other months, false for unselectable
						showWeek: false, // True to show week of the year, false to not show it
						calculateWeek: this.iso8601Week, // How to calculate the week of the year,
						// takes a Date and returns the number of the week for it
						shortYearCutoff: "+10", // Short year values < this are in the current century,
						// > this are in the previous century,
						// string value starting with "+" for current year + value
						minDate: null, // The earliest selectable date, or null for no limit
						maxDate: null, // The latest selectable date, or null for no limit
						duration: "fast", // Duration of display/closure
						beforeShowDay: null, // Function that takes a date and returns an array with
						// [0] = true if selectable, false if not, [1] = custom CSS class name(s) or "",
						// [2] = cell title (optional), e.g. $.datepicker.noWeekends
						beforeShow: null, // Function that takes an input field and
						// returns a set of custom settings for the date picker
						onSelect: null, // Define a callback function when a date is selected
						onChangeMonthYear: null, // Define a callback function when the month or year is changed
						onClose: null, // Define a callback function when the datepicker is closed
						numberOfMonths: 1, // Number of months to show at a time
						showCurrentAtPos: 0, // The position in multipe months at which to show the current month (starting at 0)
						stepMonths: 1, // Number of months to step back/forward
						stepBigMonths: 12, // Number of months to step back/forward for the big links
						altField: "", // Selector for an alternate field to store selected dates into
						altFormat: "", // The date format to use for the alternate field
						constrainInput: true, // The input is constrained by the current date format
						showButtonPanel: false, // True to show button panel, false to not show it
						autoSize: false, // True to size the input for the date format, false to leave as is
						disabled: false // The initial disabled state
					};
					$.extend( this._defaults, this.regional[ "" ] );
					this.regional.en = $.extend( true, {}, this.regional[ "" ] );
					this.regional[ "en-US" ] = $.extend( true, {}, this.regional.en );
					this.dpDiv = datepicker_bindHover( $( "<div id='" + this._mainDivId + "' class='ui-datepicker ui-widget ui-widget-content ui-helper-clearfix ui-corner-all'></div>" ) );
				}

				$.extend( Datepicker.prototype, {
					/* Class name added to elements to indicate already configured with a date picker. */
					markerClassName: "hasDatepicker",

					//Keep track of the maximum number of rows displayed (see #7043)
					maxRows: 4,

					// TODO rename to "widget" when switching to widget factory
					_widgetDatepicker: function() {
						return this.dpDiv;
					},

					/* Override the default settings for all instances of the date picker.
	 * @param  settings  object - the new settings to use as defaults (anonymous object)
	 * @return the manager object
	 */
					setDefaults: function( settings ) {
						datepicker_extendRemove( this._defaults, settings || {} );
						return this;
					},

					/* Attach the date picker to a jQuery selection.
	 * @param  target	element - the target input field or division or span
	 * @param  settings  object - the new settings to use for this date picker instance (anonymous)
	 */
					_attachDatepicker: function( target, settings ) {
						var nodeName, inline, inst;
						nodeName = target.nodeName.toLowerCase();
						inline = ( nodeName === "div" || nodeName === "span" );
						if ( !target.id ) {
							this.uuid += 1;
							target.id = "dp" + this.uuid;
						}
						inst = this._newInst( $( target ), inline );
						inst.settings = $.extend( {}, settings || {} );
						if ( nodeName === "input" ) {
							this._connectDatepicker( target, inst );
						} else if ( inline ) {
							this._inlineDatepicker( target, inst );
						}
					},

					/* Create a new instance object. */
					_newInst: function( target, inline ) {
						var id = target[ 0 ].id.replace( /([^A-Za-z0-9_\-])/g, "\\\\$1" ); // escape jQuery meta chars
						return { id: id, input: target, // associated target
							selectedDay: 0, selectedMonth: 0, selectedYear: 0, // current selection
							drawMonth: 0, drawYear: 0, // month being drawn
							inline: inline, // is datepicker inline or not
							dpDiv: ( !inline ? this.dpDiv : // presentation div
								datepicker_bindHover( $( "<div class='" + this._inlineClass + " ui-datepicker ui-widget ui-widget-content ui-helper-clearfix ui-corner-all'></div>" ) ) ) };
					},

					/* Attach the date picker to an input field. */
					_connectDatepicker: function( target, inst ) {
						var input = $( target );
						inst.append = $( [] );
						inst.trigger = $( [] );
						if ( input.hasClass( this.markerClassName ) ) {
							return;
						}
						this._attachments( input, inst );
						input.addClass( this.markerClassName ).on( "keydown", this._doKeyDown ).
						on( "keypress", this._doKeyPress ).on( "keyup", this._doKeyUp );
						this._autoSize( inst );
						$.data( target, "datepicker", inst );

						//If disabled option is true, disable the datepicker once it has been attached to the input (see ticket #5665)
						if ( inst.settings.disabled ) {
							this._disableDatepicker( target );
						}
					},

					/* Make attachments based on settings. */
					_attachments: function( input, inst ) {
						var showOn, buttonText, buttonImage,
							appendText = this._get( inst, "appendText" ),
							isRTL = this._get( inst, "isRTL" );

						if ( inst.append ) {
							inst.append.remove();
						}
						if ( appendText ) {
							inst.append = $( "<span class='" + this._appendClass + "'>" + appendText + "</span>" );
							input[ isRTL ? "before" : "after" ]( inst.append );
						}

						input.off( "focus", this._showDatepicker );

						if ( inst.trigger ) {
							inst.trigger.remove();
						}

						showOn = this._get( inst, "showOn" );
						if ( showOn === "focus" || showOn === "both" ) { // pop-up date picker when in the marked field
							input.on( "focus", this._showDatepicker );
						}
						if ( showOn === "button" || showOn === "both" ) { // pop-up date picker when button clicked
							buttonText = this._get( inst, "buttonText" );
							buttonImage = this._get( inst, "buttonImage" );
							inst.trigger = $( this._get( inst, "buttonImageOnly" ) ?
								$( "<img/>" ).addClass( this._triggerClass ).
								attr( { src: buttonImage, alt: buttonText, title: buttonText } ) :
								$( "<button type='button'></button>" ).addClass( this._triggerClass ).
								html( !buttonImage ? buttonText : $( "<img/>" ).attr(
									{ src:buttonImage, alt:buttonText, title:buttonText } ) ) );
							input[ isRTL ? "before" : "after" ]( inst.trigger );
							inst.trigger.on( "click", function() {
								if ( $.datepicker._datepickerShowing && $.datepicker._lastInput === input[ 0 ] ) {
									$.datepicker._hideDatepicker();
								} else if ( $.datepicker._datepickerShowing && $.datepicker._lastInput !== input[ 0 ] ) {
									$.datepicker._hideDatepicker();
									$.datepicker._showDatepicker( input[ 0 ] );
								} else {
									$.datepicker._showDatepicker( input[ 0 ] );
								}
								return false;
							} );
						}
					},

					/* Apply the maximum length for the date format. */
					_autoSize: function( inst ) {
						if ( this._get( inst, "autoSize" ) && !inst.inline ) {
							var findMax, max, maxI, i,
								date = new Date( 2009, 12 - 1, 20 ), // Ensure double digits
								dateFormat = this._get( inst, "dateFormat" );

							if ( dateFormat.match( /[DM]/ ) ) {
								findMax = function( names ) {
									max = 0;
									maxI = 0;
									for ( i = 0; i < names.length; i++ ) {
										if ( names[ i ].length > max ) {
											max = names[ i ].length;
											maxI = i;
										}
									}
									return maxI;
								};
								date.setMonth( findMax( this._get( inst, ( dateFormat.match( /MM/ ) ?
									"monthNames" : "monthNamesShort" ) ) ) );
								date.setDate( findMax( this._get( inst, ( dateFormat.match( /DD/ ) ?
									"dayNames" : "dayNamesShort" ) ) ) + 20 - date.getDay() );
							}
							inst.input.attr( "size", this._formatDate( inst, date ).length );
						}
					},

					/* Attach an inline date picker to a div. */
					_inlineDatepicker: function( target, inst ) {
						var divSpan = $( target );
						if ( divSpan.hasClass( this.markerClassName ) ) {
							return;
						}
						divSpan.addClass( this.markerClassName ).append( inst.dpDiv );
						$.data( target, "datepicker", inst );
						this._setDate( inst, this._getDefaultDate( inst ), true );
						this._updateDatepicker( inst );
						this._updateAlternate( inst );

						//If disabled option is true, disable the datepicker before showing it (see ticket #5665)
						if ( inst.settings.disabled ) {
							this._disableDatepicker( target );
						}

						// Set display:block in place of inst.dpDiv.show() which won't work on disconnected elements
						// http://bugs.jqueryui.com/ticket/7552 - A Datepicker created on a detached div has zero height
						inst.dpDiv.css( "display", "block" );
					},

					/* Pop-up the date picker in a "dialog" box.
	 * @param  input element - ignored
	 * @param  date	string or Date - the initial date to display
	 * @param  onSelect  function - the function to call when a date is selected
	 * @param  settings  object - update the dialog date picker instance's settings (anonymous object)
	 * @param  pos int[2] - coordinates for the dialog's position within the screen or
	 *					event - with x/y coordinates or
	 *					leave empty for default (screen centre)
	 * @return the manager object
	 */
					_dialogDatepicker: function( input, date, onSelect, settings, pos ) {
						var id, browserWidth, browserHeight, scrollX, scrollY,
							inst = this._dialogInst; // internal instance

						if ( !inst ) {
							this.uuid += 1;
							id = "dp" + this.uuid;
							this._dialogInput = $( "<input type='text' id='" + id +
								"' style='position: absolute; top: -100px; width: 0px;'/>" );
							this._dialogInput.on( "keydown", this._doKeyDown );
							$( "body" ).append( this._dialogInput );
							inst = this._dialogInst = this._newInst( this._dialogInput, false );
							inst.settings = {};
							$.data( this._dialogInput[ 0 ], "datepicker", inst );
						}
						datepicker_extendRemove( inst.settings, settings || {} );
						date = ( date && date.constructor === Date ? this._formatDate( inst, date ) : date );
						this._dialogInput.val( date );

						this._pos = ( pos ? ( pos.length ? pos : [ pos.pageX, pos.pageY ] ) : null );
						if ( !this._pos ) {
							browserWidth = document.documentElement.clientWidth;
							browserHeight = document.documentElement.clientHeight;
							scrollX = document.documentElement.scrollLeft || document.body.scrollLeft;
							scrollY = document.documentElement.scrollTop || document.body.scrollTop;
							this._pos = // should use actual width/height below
								[ ( browserWidth / 2 ) - 100 + scrollX, ( browserHeight / 2 ) - 150 + scrollY ];
						}

						// Move input on screen for focus, but hidden behind dialog
						this._dialogInput.css( "left", ( this._pos[ 0 ] + 20 ) + "px" ).css( "top", this._pos[ 1 ] + "px" );
						inst.settings.onSelect = onSelect;
						this._inDialog = true;
						this.dpDiv.addClass( this._dialogClass );
						this._showDatepicker( this._dialogInput[ 0 ] );
						if ( $.blockUI ) {
							$.blockUI( this.dpDiv );
						}
						$.data( this._dialogInput[ 0 ], "datepicker", inst );
						return this;
					},

					/* Detach a datepicker from its control.
	 * @param  target	element - the target input field or division or span
	 */
					_destroyDatepicker: function( target ) {
						var nodeName,
							$target = $( target ),
							inst = $.data( target, "datepicker" );

						if ( !$target.hasClass( this.markerClassName ) ) {
							return;
						}

						nodeName = target.nodeName.toLowerCase();
						$.removeData( target, "datepicker" );
						if ( nodeName === "input" ) {
							inst.append.remove();
							inst.trigger.remove();
							$target.removeClass( this.markerClassName ).
							off( "focus", this._showDatepicker ).
							off( "keydown", this._doKeyDown ).
							off( "keypress", this._doKeyPress ).
							off( "keyup", this._doKeyUp );
						} else if ( nodeName === "div" || nodeName === "span" ) {
							$target.removeClass( this.markerClassName ).empty();
						}

						if ( datepicker_instActive === inst ) {
							datepicker_instActive = null;
						}
					},

					/* Enable the date picker to a jQuery selection.
	 * @param  target	element - the target input field or division or span
	 */
					_enableDatepicker: function( target ) {
						var nodeName, inline,
							$target = $( target ),
							inst = $.data( target, "datepicker" );

						if ( !$target.hasClass( this.markerClassName ) ) {
							return;
						}

						nodeName = target.nodeName.toLowerCase();
						if ( nodeName === "input" ) {
							target.disabled = false;
							inst.trigger.filter( "button" ).
							each( function() { this.disabled = false; } ).end().
							filter( "img" ).css( { opacity: "1.0", cursor: "" } );
						} else if ( nodeName === "div" || nodeName === "span" ) {
							inline = $target.children( "." + this._inlineClass );
							inline.children().removeClass( "ui-state-disabled" );
							inline.find( "select.ui-datepicker-month, select.ui-datepicker-year" ).
							prop( "disabled", false );
						}
						this._disabledInputs = $.map( this._disabledInputs,
							function( value ) { return ( value === target ? null : value ); } ); // delete entry
					},

					/* Disable the date picker to a jQuery selection.
	 * @param  target	element - the target input field or division or span
	 */
					_disableDatepicker: function( target ) {
						var nodeName, inline,
							$target = $( target ),
							inst = $.data( target, "datepicker" );

						if ( !$target.hasClass( this.markerClassName ) ) {
							return;
						}

						nodeName = target.nodeName.toLowerCase();
						if ( nodeName === "input" ) {
							target.disabled = true;
							inst.trigger.filter( "button" ).
							each( function() { this.disabled = true; } ).end().
							filter( "img" ).css( { opacity: "0.5", cursor: "default" } );
						} else if ( nodeName === "div" || nodeName === "span" ) {
							inline = $target.children( "." + this._inlineClass );
							inline.children().addClass( "ui-state-disabled" );
							inline.find( "select.ui-datepicker-month, select.ui-datepicker-year" ).
							prop( "disabled", true );
						}
						this._disabledInputs = $.map( this._disabledInputs,
							function( value ) { return ( value === target ? null : value ); } ); // delete entry
						this._disabledInputs[ this._disabledInputs.length ] = target;
					},

					/* Is the first field in a jQuery collection disabled as a datepicker?
	 * @param  target	element - the target input field or division or span
	 * @return boolean - true if disabled, false if enabled
	 */
					_isDisabledDatepicker: function( target ) {
						if ( !target ) {
							return false;
						}
						for ( var i = 0; i < this._disabledInputs.length; i++ ) {
							if ( this._disabledInputs[ i ] === target ) {
								return true;
							}
						}
						return false;
					},

					/* Retrieve the instance data for the target control.
	 * @param  target  element - the target input field or division or span
	 * @return  object - the associated instance data
	 * @throws  error if a jQuery problem getting data
	 */
					_getInst: function( target ) {
						try {
							return $.data( target, "datepicker" );
						}
						catch ( err ) {
							throw "Missing instance data for this datepicker";
						}
					},

					/* Update or retrieve the settings for a date picker attached to an input field or division.
	 * @param  target  element - the target input field or division or span
	 * @param  name	object - the new settings to update or
	 *				string - the name of the setting to change or retrieve,
	 *				when retrieving also "all" for all instance settings or
	 *				"defaults" for all global defaults
	 * @param  value   any - the new value for the setting
	 *				(omit if above is an object or to retrieve a value)
	 */
					_optionDatepicker: function( target, name, value ) {
						var settings, date, minDate, maxDate,
							inst = this._getInst( target );

						if ( arguments.length === 2 && typeof name === "string" ) {
							return ( name === "defaults" ? $.extend( {}, $.datepicker._defaults ) :
								( inst ? ( name === "all" ? $.extend( {}, inst.settings ) :
									this._get( inst, name ) ) : null ) );
						}

						settings = name || {};
						if ( typeof name === "string" ) {
							settings = {};
							settings[ name ] = value;
						}

						if ( inst ) {
							if ( this._curInst === inst ) {
								this._hideDatepicker();
							}

							date = this._getDateDatepicker( target, true );
							minDate = this._getMinMaxDate( inst, "min" );
							maxDate = this._getMinMaxDate( inst, "max" );
							datepicker_extendRemove( inst.settings, settings );

							// reformat the old minDate/maxDate values if dateFormat changes and a new minDate/maxDate isn't provided
							if ( minDate !== null && settings.dateFormat !== undefined && settings.minDate === undefined ) {
								inst.settings.minDate = this._formatDate( inst, minDate );
							}
							if ( maxDate !== null && settings.dateFormat !== undefined && settings.maxDate === undefined ) {
								inst.settings.maxDate = this._formatDate( inst, maxDate );
							}
							if ( "disabled" in settings ) {
								if ( settings.disabled ) {
									this._disableDatepicker( target );
								} else {
									this._enableDatepicker( target );
								}
							}
							this._attachments( $( target ), inst );
							this._autoSize( inst );
							this._setDate( inst, date );
							this._updateAlternate( inst );
							this._updateDatepicker( inst );
						}
					},

					// Change method deprecated
					_changeDatepicker: function( target, name, value ) {
						this._optionDatepicker( target, name, value );
					},

					/* Redraw the date picker attached to an input field or division.
	 * @param  target  element - the target input field or division or span
	 */
					_refreshDatepicker: function( target ) {
						var inst = this._getInst( target );
						if ( inst ) {
							this._updateDatepicker( inst );
						}
					},

					/* Set the dates for a jQuery selection.
	 * @param  target element - the target input field or division or span
	 * @param  date	Date - the new date
	 */
					_setDateDatepicker: function( target, date ) {
						var inst = this._getInst( target );
						if ( inst ) {
							this._setDate( inst, date );
							this._updateDatepicker( inst );
							this._updateAlternate( inst );
						}
					},

					/* Get the date(s) for the first entry in a jQuery selection.
	 * @param  target element - the target input field or division or span
	 * @param  noDefault boolean - true if no default date is to be used
	 * @return Date - the current date
	 */
					_getDateDatepicker: function( target, noDefault ) {
						var inst = this._getInst( target );
						if ( inst && !inst.inline ) {
							this._setDateFromField( inst, noDefault );
						}
						return ( inst ? this._getDate( inst ) : null );
					},

					/* Handle keystrokes. */
					_doKeyDown: function( event ) {
						var onSelect, dateStr, sel,
							inst = $.datepicker._getInst( event.target ),
							handled = true,
							isRTL = inst.dpDiv.is( ".ui-datepicker-rtl" );

						inst._keyEvent = true;
						if ( $.datepicker._datepickerShowing ) {
							switch ( event.keyCode ) {
								case 9: $.datepicker._hideDatepicker();
									handled = false;
									break; // hide on tab out
								case 13: sel = $( "td." + $.datepicker._dayOverClass + ":not(." +
									$.datepicker._currentClass + ")", inst.dpDiv );
									if ( sel[ 0 ] ) {
										$.datepicker._selectDay( event.target, inst.selectedMonth, inst.selectedYear, sel[ 0 ] );
									}

									onSelect = $.datepicker._get( inst, "onSelect" );
									if ( onSelect ) {
										dateStr = $.datepicker._formatDate( inst );

										// Trigger custom callback
										onSelect.apply( ( inst.input ? inst.input[ 0 ] : null ), [ dateStr, inst ] );
									} else {
										$.datepicker._hideDatepicker();
									}

									return false; // don't submit the form
								case 27: $.datepicker._hideDatepicker();
									break; // hide on escape
								case 33: $.datepicker._adjustDate( event.target, ( event.ctrlKey ?
									-$.datepicker._get( inst, "stepBigMonths" ) :
									-$.datepicker._get( inst, "stepMonths" ) ), "M" );
									break; // previous month/year on page up/+ ctrl
								case 34: $.datepicker._adjustDate( event.target, ( event.ctrlKey ?
									+$.datepicker._get( inst, "stepBigMonths" ) :
									+$.datepicker._get( inst, "stepMonths" ) ), "M" );
									break; // next month/year on page down/+ ctrl
								case 35: if ( event.ctrlKey || event.metaKey ) {
									$.datepicker._clearDate( event.target );
								}
									handled = event.ctrlKey || event.metaKey;
									break; // clear on ctrl or command +end
								case 36: if ( event.ctrlKey || event.metaKey ) {
									$.datepicker._gotoToday( event.target );
								}
									handled = event.ctrlKey || event.metaKey;
									break; // current on ctrl or command +home
								case 37: if ( event.ctrlKey || event.metaKey ) {
									$.datepicker._adjustDate( event.target, ( isRTL ? +1 : -1 ), "D" );
								}
									handled = event.ctrlKey || event.metaKey;

									// -1 day on ctrl or command +left
									if ( event.originalEvent.altKey ) {
										$.datepicker._adjustDate( event.target, ( event.ctrlKey ?
											-$.datepicker._get( inst, "stepBigMonths" ) :
											-$.datepicker._get( inst, "stepMonths" ) ), "M" );
									}

									// next month/year on alt +left on Mac
									break;
								case 38: if ( event.ctrlKey || event.metaKey ) {
									$.datepicker._adjustDate( event.target, -7, "D" );
								}
									handled = event.ctrlKey || event.metaKey;
									break; // -1 week on ctrl or command +up
								case 39: if ( event.ctrlKey || event.metaKey ) {
									$.datepicker._adjustDate( event.target, ( isRTL ? -1 : +1 ), "D" );
								}
									handled = event.ctrlKey || event.metaKey;

									// +1 day on ctrl or command +right
									if ( event.originalEvent.altKey ) {
										$.datepicker._adjustDate( event.target, ( event.ctrlKey ?
											+$.datepicker._get( inst, "stepBigMonths" ) :
											+$.datepicker._get( inst, "stepMonths" ) ), "M" );
									}

									// next month/year on alt +right
									break;
								case 40: if ( event.ctrlKey || event.metaKey ) {
									$.datepicker._adjustDate( event.target, +7, "D" );
								}
									handled = event.ctrlKey || event.metaKey;
									break; // +1 week on ctrl or command +down
								default: handled = false;
							}
						} else if ( event.keyCode === 36 && event.ctrlKey ) { // display the date picker on ctrl+home
							$.datepicker._showDatepicker( this );
						} else {
							handled = false;
						}

						if ( handled ) {
							event.preventDefault();
							event.stopPropagation();
						}
					},

					/* Filter entered characters - based on date format. */
					_doKeyPress: function( event ) {
						var chars, chr,
							inst = $.datepicker._getInst( event.target );

						if ( $.datepicker._get( inst, "constrainInput" ) ) {
							chars = $.datepicker._possibleChars( $.datepicker._get( inst, "dateFormat" ) );
							chr = String.fromCharCode( event.charCode == null ? event.keyCode : event.charCode );
							return event.ctrlKey || event.metaKey || ( chr < " " || !chars || chars.indexOf( chr ) > -1 );
						}
					},

					/* Synchronise manual entry and field/alternate field. */
					_doKeyUp: function( event ) {
						var date,
							inst = $.datepicker._getInst( event.target );

						if ( inst.input.val() !== inst.lastVal ) {
							try {
								date = $.datepicker.parseDate( $.datepicker._get( inst, "dateFormat" ),
									( inst.input ? inst.input.val() : null ),
									$.datepicker._getFormatConfig( inst ) );

								if ( date ) { // only if valid
									$.datepicker._setDateFromField( inst );
									$.datepicker._updateAlternate( inst );
									$.datepicker._updateDatepicker( inst );
								}
							}
							catch ( err ) {
							}
						}
						return true;
					},

					/* Pop-up the date picker for a given input field.
	 * If false returned from beforeShow event handler do not show.
	 * @param  input  element - the input field attached to the date picker or
	 *					event - if triggered by focus
	 */
					_showDatepicker: function( input ) {
						input = input.target || input;
						if ( input.nodeName.toLowerCase() !== "input" ) { // find from button/image trigger
							input = $( "input", input.parentNode )[ 0 ];
						}

						if ( $.datepicker._isDisabledDatepicker( input ) || $.datepicker._lastInput === input ) { // already here
							return;
						}

						var inst, beforeShow, beforeShowSettings, isFixed,
							offset, showAnim, duration;

						inst = $.datepicker._getInst( input );
						if ( $.datepicker._curInst && $.datepicker._curInst !== inst ) {
							$.datepicker._curInst.dpDiv.stop( true, true );
							if ( inst && $.datepicker._datepickerShowing ) {
								$.datepicker._hideDatepicker( $.datepicker._curInst.input[ 0 ] );
							}
						}

						beforeShow = $.datepicker._get( inst, "beforeShow" );
						beforeShowSettings = beforeShow ? beforeShow.apply( input, [ input, inst ] ) : {};
						if ( beforeShowSettings === false ) {
							return;
						}
						datepicker_extendRemove( inst.settings, beforeShowSettings );

						inst.lastVal = null;
						$.datepicker._lastInput = input;
						$.datepicker._setDateFromField( inst );

						if ( $.datepicker._inDialog ) { // hide cursor
							input.value = "";
						}
						if ( !$.datepicker._pos ) { // position below input
							$.datepicker._pos = $.datepicker._findPos( input );
							$.datepicker._pos[ 1 ] += input.offsetHeight; // add the height
						}

						isFixed = false;
						$( input ).parents().each( function() {
							isFixed |= $( this ).css( "position" ) === "fixed";
							return !isFixed;
						} );

						offset = { left: $.datepicker._pos[ 0 ], top: $.datepicker._pos[ 1 ] };
						$.datepicker._pos = null;

						//to avoid flashes on Firefox
						inst.dpDiv.empty();

						// determine sizing offscreen
						inst.dpDiv.css( { position: "absolute", display: "block", top: "-1000px" } );
						$.datepicker._updateDatepicker( inst );

						// fix width for dynamic number of date pickers
						// and adjust position before showing
						offset = $.datepicker._checkOffset( inst, offset, isFixed );
						inst.dpDiv.css( { position: ( $.datepicker._inDialog && $.blockUI ?
								"static" : ( isFixed ? "fixed" : "absolute" ) ), display: "none",
							left: offset.left + "px", top: offset.top + "px" } );

						if ( !inst.inline ) {
							showAnim = $.datepicker._get( inst, "showAnim" );
							duration = $.datepicker._get( inst, "duration" );
							inst.dpDiv.css( "z-index", datepicker_getZindex( $( input ) ) + 1 );
							$.datepicker._datepickerShowing = true;

							if ( $.effects && $.effects.effect[ showAnim ] ) {
								inst.dpDiv.show( showAnim, $.datepicker._get( inst, "showOptions" ), duration );
							} else {
								inst.dpDiv[ showAnim || "show" ]( showAnim ? duration : null );
							}

							if ( $.datepicker._shouldFocusInput( inst ) ) {
								inst.input.trigger( "focus" );
							}

							$.datepicker._curInst = inst;
						}
					},

					/* Generate the date picker content. */
					_updateDatepicker: function( inst ) {
						this.maxRows = 4; //Reset the max number of rows being displayed (see #7043)
						datepicker_instActive = inst; // for delegate hover events
						inst.dpDiv.empty().append( this._generateHTML( inst ) );
						this._attachHandlers( inst );

						var origyearshtml,
							numMonths = this._getNumberOfMonths( inst ),
							cols = numMonths[ 1 ],
							width = 17,
							activeCell = inst.dpDiv.find( "." + this._dayOverClass + " a" );

						if ( activeCell.length > 0 ) {
							datepicker_handleMouseover.apply( activeCell.get( 0 ) );
						}

						inst.dpDiv.removeClass( "ui-datepicker-multi-2 ui-datepicker-multi-3 ui-datepicker-multi-4" ).width( "" );
						if ( cols > 1 ) {
							inst.dpDiv.addClass( "ui-datepicker-multi-" + cols ).css( "width", ( width * cols ) + "em" );
						}
						inst.dpDiv[ ( numMonths[ 0 ] !== 1 || numMonths[ 1 ] !== 1 ? "add" : "remove" ) +
						"Class" ]( "ui-datepicker-multi" );
						inst.dpDiv[ ( this._get( inst, "isRTL" ) ? "add" : "remove" ) +
						"Class" ]( "ui-datepicker-rtl" );

						if ( inst === $.datepicker._curInst && $.datepicker._datepickerShowing && $.datepicker._shouldFocusInput( inst ) ) {
							inst.input.trigger( "focus" );
						}

						// Deffered render of the years select (to avoid flashes on Firefox)
						if ( inst.yearshtml ) {
							origyearshtml = inst.yearshtml;
							setTimeout( function() {

								//assure that inst.yearshtml didn't change.
								if ( origyearshtml === inst.yearshtml && inst.yearshtml ) {
									inst.dpDiv.find( "select.ui-datepicker-year:first" ).replaceWith( inst.yearshtml );
								}
								origyearshtml = inst.yearshtml = null;
							}, 0 );
						}
					},

					// #6694 - don't focus the input if it's already focused
					// this breaks the change event in IE
					// Support: IE and jQuery <1.9
					_shouldFocusInput: function( inst ) {
						return inst.input && inst.input.is( ":visible" ) && !inst.input.is( ":disabled" ) && !inst.input.is( ":focus" );
					},

					/* Check positioning to remain on screen. */
					_checkOffset: function( inst, offset, isFixed ) {
						var dpWidth = inst.dpDiv.outerWidth(),
							dpHeight = inst.dpDiv.outerHeight(),
							inputWidth = inst.input ? inst.input.outerWidth() : 0,
							inputHeight = inst.input ? inst.input.outerHeight() : 0,
							viewWidth = document.documentElement.clientWidth + ( isFixed ? 0 : $( document ).scrollLeft() ),
							viewHeight = document.documentElement.clientHeight + ( isFixed ? 0 : $( document ).scrollTop() );

						offset.left -= ( this._get( inst, "isRTL" ) ? ( dpWidth - inputWidth ) : 0 );
						offset.left -= ( isFixed && offset.left === inst.input.offset().left ) ? $( document ).scrollLeft() : 0;
						offset.top -= ( isFixed && offset.top === ( inst.input.offset().top + inputHeight ) ) ? $( document ).scrollTop() : 0;

						// Now check if datepicker is showing outside window viewport - move to a better place if so.
						offset.left -= Math.min( offset.left, ( offset.left + dpWidth > viewWidth && viewWidth > dpWidth ) ?
							Math.abs( offset.left + dpWidth - viewWidth ) : 0 );
						offset.top -= Math.min( offset.top, ( offset.top + dpHeight > viewHeight && viewHeight > dpHeight ) ?
							Math.abs( dpHeight + inputHeight ) : 0 );

						return offset;
					},

					/* Find an object's position on the screen. */
					_findPos: function( obj ) {
						var position,
							inst = this._getInst( obj ),
							isRTL = this._get( inst, "isRTL" );

						while ( obj && ( obj.type === "hidden" || obj.nodeType !== 1 || $.expr.filters.hidden( obj ) ) ) {
							obj = obj[ isRTL ? "previousSibling" : "nextSibling" ];
						}

						position = $( obj ).offset();
						return [ position.left, position.top ];
					},

					/* Hide the date picker from view.
	 * @param  input  element - the input field attached to the date picker
	 */
					_hideDatepicker: function( input ) {
						var showAnim, duration, postProcess, onClose,
							inst = this._curInst;

						if ( !inst || ( input && inst !== $.data( input, "datepicker" ) ) ) {
							return;
						}

						if ( this._datepickerShowing ) {
							showAnim = this._get( inst, "showAnim" );
							duration = this._get( inst, "duration" );
							postProcess = function() {
								$.datepicker._tidyDialog( inst );
							};

							// DEPRECATED: after BC for 1.8.x $.effects[ showAnim ] is not needed
							if ( $.effects && ( $.effects.effect[ showAnim ] || $.effects[ showAnim ] ) ) {
								inst.dpDiv.hide( showAnim, $.datepicker._get( inst, "showOptions" ), duration, postProcess );
							} else {
								inst.dpDiv[ ( showAnim === "slideDown" ? "slideUp" :
									( showAnim === "fadeIn" ? "fadeOut" : "hide" ) ) ]( ( showAnim ? duration : null ), postProcess );
							}

							if ( !showAnim ) {
								postProcess();
							}
							this._datepickerShowing = false;

							onClose = this._get( inst, "onClose" );
							if ( onClose ) {
								onClose.apply( ( inst.input ? inst.input[ 0 ] : null ), [ ( inst.input ? inst.input.val() : "" ), inst ] );
							}

							this._lastInput = null;
							if ( this._inDialog ) {
								this._dialogInput.css( { position: "absolute", left: "0", top: "-100px" } );
								if ( $.blockUI ) {
									$.unblockUI();
									$( "body" ).append( this.dpDiv );
								}
							}
							this._inDialog = false;
						}
					},

					/* Tidy up after a dialog display. */
					_tidyDialog: function( inst ) {
						inst.dpDiv.removeClass( this._dialogClass ).off( ".ui-datepicker-calendar" );
					},

					/* Close date picker if clicked elsewhere. */
					_checkExternalClick: function( event ) {
						if ( !$.datepicker._curInst ) {
							return;
						}

						var $target = $( event.target ),
							inst = $.datepicker._getInst( $target[ 0 ] );

						if ( ( ( $target[ 0 ].id !== $.datepicker._mainDivId &&
							$target.parents( "#" + $.datepicker._mainDivId ).length === 0 &&
							!$target.hasClass( $.datepicker.markerClassName ) &&
							!$target.closest( "." + $.datepicker._triggerClass ).length &&
							$.datepicker._datepickerShowing && !( $.datepicker._inDialog && $.blockUI ) ) ) ||
							( $target.hasClass( $.datepicker.markerClassName ) && $.datepicker._curInst !== inst ) ) {
							$.datepicker._hideDatepicker();
						}
					},

					/* Adjust one of the date sub-fields. */
					_adjustDate: function( id, offset, period ) {
						var target = $( id ),
							inst = this._getInst( target[ 0 ] );

						if ( this._isDisabledDatepicker( target[ 0 ] ) ) {
							return;
						}
						this._adjustInstDate( inst, offset +
							( period === "M" ? this._get( inst, "showCurrentAtPos" ) : 0 ), // undo positioning
							period );
						this._updateDatepicker( inst );
					},

					/* Action for current link. */
					_gotoToday: function( id ) {
						var date,
							target = $( id ),
							inst = this._getInst( target[ 0 ] );

						if ( this._get( inst, "gotoCurrent" ) && inst.currentDay ) {
							inst.selectedDay = inst.currentDay;
							inst.drawMonth = inst.selectedMonth = inst.currentMonth;
							inst.drawYear = inst.selectedYear = inst.currentYear;
						} else {
							date = new Date();
							inst.selectedDay = date.getDate();
							inst.drawMonth = inst.selectedMonth = date.getMonth();
							inst.drawYear = inst.selectedYear = date.getFullYear();
						}
						this._notifyChange( inst );
						this._adjustDate( target );
					},

					/* Action for selecting a new month/year. */
					_selectMonthYear: function( id, select, period ) {
						var target = $( id ),
							inst = this._getInst( target[ 0 ] );

						inst[ "selected" + ( period === "M" ? "Month" : "Year" ) ] =
							inst[ "draw" + ( period === "M" ? "Month" : "Year" ) ] =
								parseInt( select.options[ select.selectedIndex ].value, 10 );

						this._notifyChange( inst );
						this._adjustDate( target );
					},

					/* Action for selecting a day. */
					_selectDay: function( id, month, year, td ) {
						var inst,
							target = $( id );

						if ( $( td ).hasClass( this._unselectableClass ) || this._isDisabledDatepicker( target[ 0 ] ) ) {
							return;
						}

						inst = this._getInst( target[ 0 ] );
						inst.selectedDay = inst.currentDay = $( "a", td ).html();
						inst.selectedMonth = inst.currentMonth = month;
						inst.selectedYear = inst.currentYear = year;
						this._selectDate( id, this._formatDate( inst,
							inst.currentDay, inst.currentMonth, inst.currentYear ) );
					},

					/* Erase the input field and hide the date picker. */
					_clearDate: function( id ) {
						var target = $( id );
						this._selectDate( target, "" );
					},

					/* Update the input field with the selected date. */
					_selectDate: function( id, dateStr ) {
						var onSelect,
							target = $( id ),
							inst = this._getInst( target[ 0 ] );

						dateStr = ( dateStr != null ? dateStr : this._formatDate( inst ) );
						if ( inst.input ) {
							inst.input.val( dateStr );
						}
						this._updateAlternate( inst );

						onSelect = this._get( inst, "onSelect" );
						if ( onSelect ) {
							onSelect.apply( ( inst.input ? inst.input[ 0 ] : null ), [ dateStr, inst ] );  // trigger custom callback
						} else if ( inst.input ) {
							inst.input.trigger( "change" ); // fire the change event
						}

						if ( inst.inline ) {
							this._updateDatepicker( inst );
						} else {
							this._hideDatepicker();
							this._lastInput = inst.input[ 0 ];
							if ( typeof( inst.input[ 0 ] ) !== "object" ) {
								inst.input.trigger( "focus" ); // restore focus
							}
							this._lastInput = null;
						}
					},

					/* Update any alternate field to synchronise with the main field. */
					_updateAlternate: function( inst ) {
						var altFormat, date, dateStr,
							altField = this._get( inst, "altField" );

						if ( altField ) { // update alternate field too
							altFormat = this._get( inst, "altFormat" ) || this._get( inst, "dateFormat" );
							date = this._getDate( inst );
							dateStr = this.formatDate( altFormat, date, this._getFormatConfig( inst ) );
							$( altField ).val( dateStr );
						}
					},

					/* Set as beforeShowDay function to prevent selection of weekends.
	 * @param  date  Date - the date to customise
	 * @return [boolean, string] - is this date selectable?, what is its CSS class?
	 */
					noWeekends: function( date ) {
						var day = date.getDay();
						return [ ( day > 0 && day < 6 ), "" ];
					},

					/* Set as calculateWeek to determine the week of the year based on the ISO 8601 definition.
	 * @param  date  Date - the date to get the week for
	 * @return  number - the number of the week within the year that contains this date
	 */
					iso8601Week: function( date ) {
						var time,
							checkDate = new Date( date.getTime() );

						// Find Thursday of this week starting on Monday
						checkDate.setDate( checkDate.getDate() + 4 - ( checkDate.getDay() || 7 ) );

						time = checkDate.getTime();
						checkDate.setMonth( 0 ); // Compare with Jan 1
						checkDate.setDate( 1 );
						return Math.floor( Math.round( ( time - checkDate ) / 86400000 ) / 7 ) + 1;
					},

					/* Parse a string value into a date object.
	 * See formatDate below for the possible formats.
	 *
	 * @param  format string - the expected format of the date
	 * @param  value string - the date in the above format
	 * @param  settings Object - attributes include:
	 *					shortYearCutoff  number - the cutoff year for determining the century (optional)
	 *					dayNamesShort	string[7] - abbreviated names of the days from Sunday (optional)
	 *					dayNames		string[7] - names of the days from Sunday (optional)
	 *					monthNamesShort string[12] - abbreviated names of the months (optional)
	 *					monthNames		string[12] - names of the months (optional)
	 * @return  Date - the extracted date value or null if value is blank
	 */
					parseDate: function( format, value, settings ) {
						if ( format == null || value == null ) {
							throw "Invalid arguments";
						}

						value = ( typeof value === "object" ? value.toString() : value + "" );
						if ( value === "" ) {
							return null;
						}

						var iFormat, dim, extra,
							iValue = 0,
							shortYearCutoffTemp = ( settings ? settings.shortYearCutoff : null ) || this._defaults.shortYearCutoff,
							shortYearCutoff = ( typeof shortYearCutoffTemp !== "string" ? shortYearCutoffTemp :
								new Date().getFullYear() % 100 + parseInt( shortYearCutoffTemp, 10 ) ),
							dayNamesShort = ( settings ? settings.dayNamesShort : null ) || this._defaults.dayNamesShort,
							dayNames = ( settings ? settings.dayNames : null ) || this._defaults.dayNames,
							monthNamesShort = ( settings ? settings.monthNamesShort : null ) || this._defaults.monthNamesShort,
							monthNames = ( settings ? settings.monthNames : null ) || this._defaults.monthNames,
							year = -1,
							month = -1,
							day = -1,
							doy = -1,
							literal = false,
							date,

							// Check whether a format character is doubled
							lookAhead = function( match ) {
								var matches = ( iFormat + 1 < format.length && format.charAt( iFormat + 1 ) === match );
								if ( matches ) {
									iFormat++;
								}
								return matches;
							},

							// Extract a number from the string value
							getNumber = function( match ) {
								var isDoubled = lookAhead( match ),
									size = ( match === "@" ? 14 : ( match === "!" ? 20 :
										( match === "y" && isDoubled ? 4 : ( match === "o" ? 3 : 2 ) ) ) ),
									minSize = ( match === "y" ? size : 1 ),
									digits = new RegExp( "^\\d{" + minSize + "," + size + "}" ),
									num = value.substring( iValue ).match( digits );
								if ( !num ) {
									throw "Missing number at position " + iValue;
								}
								iValue += num[ 0 ].length;
								return parseInt( num[ 0 ], 10 );
							},

							// Extract a name from the string value and convert to an index
							getName = function( match, shortNames, longNames ) {
								var index = -1,
									names = $.map( lookAhead( match ) ? longNames : shortNames, function( v, k ) {
										return [ [ k, v ] ];
									} ).sort( function( a, b ) {
										return -( a[ 1 ].length - b[ 1 ].length );
									} );

								$.each( names, function( i, pair ) {
									var name = pair[ 1 ];
									if ( value.substr( iValue, name.length ).toLowerCase() === name.toLowerCase() ) {
										index = pair[ 0 ];
										iValue += name.length;
										return false;
									}
								} );
								if ( index !== -1 ) {
									return index + 1;
								} else {
									throw "Unknown name at position " + iValue;
								}
							},

							// Confirm that a literal character matches the string value
							checkLiteral = function() {
								if ( value.charAt( iValue ) !== format.charAt( iFormat ) ) {
									throw "Unexpected literal at position " + iValue;
								}
								iValue++;
							};

						for ( iFormat = 0; iFormat < format.length; iFormat++ ) {
							if ( literal ) {
								if ( format.charAt( iFormat ) === "'" && !lookAhead( "'" ) ) {
									literal = false;
								} else {
									checkLiteral();
								}
							} else {
								switch ( format.charAt( iFormat ) ) {
									case "d":
										day = getNumber( "d" );
										break;
									case "D":
										getName( "D", dayNamesShort, dayNames );
										break;
									case "o":
										doy = getNumber( "o" );
										break;
									case "m":
										month = getNumber( "m" );
										break;
									case "M":
										month = getName( "M", monthNamesShort, monthNames );
										break;
									case "y":
										year = getNumber( "y" );
										break;
									case "@":
										date = new Date( getNumber( "@" ) );
										year = date.getFullYear();
										month = date.getMonth() + 1;
										day = date.getDate();
										break;
									case "!":
										date = new Date( ( getNumber( "!" ) - this._ticksTo1970 ) / 10000 );
										year = date.getFullYear();
										month = date.getMonth() + 1;
										day = date.getDate();
										break;
									case "'":
										if ( lookAhead( "'" ) ) {
											checkLiteral();
										} else {
											literal = true;
										}
										break;
									default:
										checkLiteral();
								}
							}
						}

						if ( iValue < value.length ) {
							extra = value.substr( iValue );
							if ( !/^\s+/.test( extra ) ) {
								throw "Extra/unparsed characters found in date: " + extra;
							}
						}

						if ( year === -1 ) {
							year = new Date().getFullYear();
						} else if ( year < 100 ) {
							year += new Date().getFullYear() - new Date().getFullYear() % 100 +
								( year <= shortYearCutoff ? 0 : -100 );
						}

						if ( doy > -1 ) {
							month = 1;
							day = doy;
							do {
								dim = this._getDaysInMonth( year, month - 1 );
								if ( day <= dim ) {
									break;
								}
								month++;
								day -= dim;
							} while ( true );
						}

						date = this._daylightSavingAdjust( new Date( year, month - 1, day ) );
						if ( date.getFullYear() !== year || date.getMonth() + 1 !== month || date.getDate() !== day ) {
							throw "Invalid date"; // E.g. 31/02/00
						}
						return date;
					},

					/* Standard date formats. */
					ATOM: "yy-mm-dd", // RFC 3339 (ISO 8601)
					COOKIE: "D, dd M yy",
					ISO_8601: "yy-mm-dd",
					RFC_822: "D, d M y",
					RFC_850: "DD, dd-M-y",
					RFC_1036: "D, d M y",
					RFC_1123: "D, d M yy",
					RFC_2822: "D, d M yy",
					RSS: "D, d M y", // RFC 822
					TICKS: "!",
					TIMESTAMP: "@",
					W3C: "yy-mm-dd", // ISO 8601

					_ticksTo1970: ( ( ( 1970 - 1 ) * 365 + Math.floor( 1970 / 4 ) - Math.floor( 1970 / 100 ) +
						Math.floor( 1970 / 400 ) ) * 24 * 60 * 60 * 10000000 ),

					/* Format a date object into a string value.
	 * The format can be combinations of the following:
	 * d  - day of month (no leading zero)
	 * dd - day of month (two digit)
	 * o  - day of year (no leading zeros)
	 * oo - day of year (three digit)
	 * D  - day name short
	 * DD - day name long
	 * m  - month of year (no leading zero)
	 * mm - month of year (two digit)
	 * M  - month name short
	 * MM - month name long
	 * y  - year (two digit)
	 * yy - year (four digit)
	 * @ - Unix timestamp (ms since 01/01/1970)
	 * ! - Windows ticks (100ns since 01/01/0001)
	 * "..." - literal text
	 * '' - single quote
	 *
	 * @param  format string - the desired format of the date
	 * @param  date Date - the date value to format
	 * @param  settings Object - attributes include:
	 *					dayNamesShort	string[7] - abbreviated names of the days from Sunday (optional)
	 *					dayNames		string[7] - names of the days from Sunday (optional)
	 *					monthNamesShort string[12] - abbreviated names of the months (optional)
	 *					monthNames		string[12] - names of the months (optional)
	 * @return  string - the date in the above format
	 */
					formatDate: function( format, date, settings ) {
						if ( !date ) {
							return "";
						}

						var iFormat,
							dayNamesShort = ( settings ? settings.dayNamesShort : null ) || this._defaults.dayNamesShort,
							dayNames = ( settings ? settings.dayNames : null ) || this._defaults.dayNames,
							monthNamesShort = ( settings ? settings.monthNamesShort : null ) || this._defaults.monthNamesShort,
							monthNames = ( settings ? settings.monthNames : null ) || this._defaults.monthNames,

							// Check whether a format character is doubled
							lookAhead = function( match ) {
								var matches = ( iFormat + 1 < format.length && format.charAt( iFormat + 1 ) === match );
								if ( matches ) {
									iFormat++;
								}
								return matches;
							},

							// Format a number, with leading zero if necessary
							formatNumber = function( match, value, len ) {
								var num = "" + value;
								if ( lookAhead( match ) ) {
									while ( num.length < len ) {
										num = "0" + num;
									}
								}
								return num;
							},

							// Format a name, short or long as requested
							formatName = function( match, value, shortNames, longNames ) {
								return ( lookAhead( match ) ? longNames[ value ] : shortNames[ value ] );
							},
							output = "",
							literal = false;

						if ( date ) {
							for ( iFormat = 0; iFormat < format.length; iFormat++ ) {
								if ( literal ) {
									if ( format.charAt( iFormat ) === "'" && !lookAhead( "'" ) ) {
										literal = false;
									} else {
										output += format.charAt( iFormat );
									}
								} else {
									switch ( format.charAt( iFormat ) ) {
										case "d":
											output += formatNumber( "d", date.getDate(), 2 );
											break;
										case "D":
											output += formatName( "D", date.getDay(), dayNamesShort, dayNames );
											break;
										case "o":
											output += formatNumber( "o",
												Math.round( ( new Date( date.getFullYear(), date.getMonth(), date.getDate() ).getTime() - new Date( date.getFullYear(), 0, 0 ).getTime() ) / 86400000 ), 3 );
											break;
										case "m":
											output += formatNumber( "m", date.getMonth() + 1, 2 );
											break;
										case "M":
											output += formatName( "M", date.getMonth(), monthNamesShort, monthNames );
											break;
										case "y":
											output += ( lookAhead( "y" ) ? date.getFullYear() :
												( date.getFullYear() % 100 < 10 ? "0" : "" ) + date.getFullYear() % 100 );
											break;
										case "@":
											output += date.getTime();
											break;
										case "!":
											output += date.getTime() * 10000 + this._ticksTo1970;
											break;
										case "'":
											if ( lookAhead( "'" ) ) {
												output += "'";
											} else {
												literal = true;
											}
											break;
										default:
											output += format.charAt( iFormat );
									}
								}
							}
						}
						return output;
					},

					/* Extract all possible characters from the date format. */
					_possibleChars: function( format ) {
						var iFormat,
							chars = "",
							literal = false,

							// Check whether a format character is doubled
							lookAhead = function( match ) {
								var matches = ( iFormat + 1 < format.length && format.charAt( iFormat + 1 ) === match );
								if ( matches ) {
									iFormat++;
								}
								return matches;
							};

						for ( iFormat = 0; iFormat < format.length; iFormat++ ) {
							if ( literal ) {
								if ( format.charAt( iFormat ) === "'" && !lookAhead( "'" ) ) {
									literal = false;
								} else {
									chars += format.charAt( iFormat );
								}
							} else {
								switch ( format.charAt( iFormat ) ) {
									case "d": case "m": case "y": case "@":
										chars += "0123456789";
										break;
									case "D": case "M":
										return null; // Accept anything
									case "'":
										if ( lookAhead( "'" ) ) {
											chars += "'";
										} else {
											literal = true;
										}
										break;
									default:
										chars += format.charAt( iFormat );
								}
							}
						}
						return chars;
					},

					/* Get a setting value, defaulting if necessary. */
					_get: function( inst, name ) {
						return inst.settings[ name ] !== undefined ?
							inst.settings[ name ] : this._defaults[ name ];
					},

					/* Parse existing date and initialise date picker. */
					_setDateFromField: function( inst, noDefault ) {
						if ( inst.input.val() === inst.lastVal ) {
							return;
						}

						var dateFormat = this._get( inst, "dateFormat" ),
							dates = inst.lastVal = inst.input ? inst.input.val() : null,
							defaultDate = this._getDefaultDate( inst ),
							date = defaultDate,
							settings = this._getFormatConfig( inst );

						try {
							date = this.parseDate( dateFormat, dates, settings ) || defaultDate;
						} catch ( event ) {
							dates = ( noDefault ? "" : dates );
						}
						inst.selectedDay = date.getDate();
						inst.drawMonth = inst.selectedMonth = date.getMonth();
						inst.drawYear = inst.selectedYear = date.getFullYear();
						inst.currentDay = ( dates ? date.getDate() : 0 );
						inst.currentMonth = ( dates ? date.getMonth() : 0 );
						inst.currentYear = ( dates ? date.getFullYear() : 0 );
						this._adjustInstDate( inst );
					},

					/* Retrieve the default date shown on opening. */
					_getDefaultDate: function( inst ) {
						return this._restrictMinMax( inst,
							this._determineDate( inst, this._get( inst, "defaultDate" ), new Date() ) );
					},

					/* A date may be specified as an exact value or a relative one. */
					_determineDate: function( inst, date, defaultDate ) {
						var offsetNumeric = function( offset ) {
								var date = new Date();
								date.setDate( date.getDate() + offset );
								return date;
							},
							offsetString = function( offset ) {
								try {
									return $.datepicker.parseDate( $.datepicker._get( inst, "dateFormat" ),
										offset, $.datepicker._getFormatConfig( inst ) );
								}
								catch ( e ) {

									// Ignore
								}

								var date = ( offset.toLowerCase().match( /^c/ ) ?
									$.datepicker._getDate( inst ) : null ) || new Date(),
									year = date.getFullYear(),
									month = date.getMonth(),
									day = date.getDate(),
									pattern = /([+\-]?[0-9]+)\s*(d|D|w|W|m|M|y|Y)?/g,
									matches = pattern.exec( offset );

								while ( matches ) {
									switch ( matches[ 2 ] || "d" ) {
										case "d" : case "D" :
											day += parseInt( matches[ 1 ], 10 ); break;
										case "w" : case "W" :
											day += parseInt( matches[ 1 ], 10 ) * 7; break;
										case "m" : case "M" :
											month += parseInt( matches[ 1 ], 10 );
											day = Math.min( day, $.datepicker._getDaysInMonth( year, month ) );
											break;
										case "y": case "Y" :
											year += parseInt( matches[ 1 ], 10 );
											day = Math.min( day, $.datepicker._getDaysInMonth( year, month ) );
											break;
									}
									matches = pattern.exec( offset );
								}
								return new Date( year, month, day );
							},
							newDate = ( date == null || date === "" ? defaultDate : ( typeof date === "string" ? offsetString( date ) :
								( typeof date === "number" ? ( isNaN( date ) ? defaultDate : offsetNumeric( date ) ) : new Date( date.getTime() ) ) ) );

						newDate = ( newDate && newDate.toString() === "Invalid Date" ? defaultDate : newDate );
						if ( newDate ) {
							newDate.setHours( 0 );
							newDate.setMinutes( 0 );
							newDate.setSeconds( 0 );
							newDate.setMilliseconds( 0 );
						}
						return this._daylightSavingAdjust( newDate );
					},

					/* Handle switch to/from daylight saving.
	 * Hours may be non-zero on daylight saving cut-over:
	 * > 12 when midnight changeover, but then cannot generate
	 * midnight datetime, so jump to 1AM, otherwise reset.
	 * @param  date  (Date) the date to check
	 * @return  (Date) the corrected date
	 */
					_daylightSavingAdjust: function( date ) {
						if ( !date ) {
							return null;
						}
						date.setHours( date.getHours() > 12 ? date.getHours() + 2 : 0 );
						return date;
					},

					/* Set the date(s) directly. */
					_setDate: function( inst, date, noChange ) {
						var clear = !date,
							origMonth = inst.selectedMonth,
							origYear = inst.selectedYear,
							newDate = this._restrictMinMax( inst, this._determineDate( inst, date, new Date() ) );

						inst.selectedDay = inst.currentDay = newDate.getDate();
						inst.drawMonth = inst.selectedMonth = inst.currentMonth = newDate.getMonth();
						inst.drawYear = inst.selectedYear = inst.currentYear = newDate.getFullYear();
						if ( ( origMonth !== inst.selectedMonth || origYear !== inst.selectedYear ) && !noChange ) {
							this._notifyChange( inst );
						}
						this._adjustInstDate( inst );
						if ( inst.input ) {
							inst.input.val( clear ? "" : this._formatDate( inst ) );
						}
					},

					/* Retrieve the date(s) directly. */
					_getDate: function( inst ) {
						var startDate = ( !inst.currentYear || ( inst.input && inst.input.val() === "" ) ? null :
							this._daylightSavingAdjust( new Date(
								inst.currentYear, inst.currentMonth, inst.currentDay ) ) );
						return startDate;
					},

					/* Attach the onxxx handlers.  These are declared statically so
	 * they work with static code transformers like Caja.
	 */
					_attachHandlers: function( inst ) {
						var stepMonths = this._get( inst, "stepMonths" ),
							id = "#" + inst.id.replace( /\\\\/g, "\\" );
						inst.dpDiv.find( "[data-handler]" ).map( function() {
							var handler = {
								prev: function() {
									$.datepicker._adjustDate( id, -stepMonths, "M" );
								},
								next: function() {
									$.datepicker._adjustDate( id, +stepMonths, "M" );
								},
								hide: function() {
									$.datepicker._hideDatepicker();
								},
								today: function() {
									$.datepicker._gotoToday( id );
								},
								selectDay: function() {
									$.datepicker._selectDay( id, +this.getAttribute( "data-month" ), +this.getAttribute( "data-year" ), this );
									return false;
								},
								selectMonth: function() {
									$.datepicker._selectMonthYear( id, this, "M" );
									return false;
								},
								selectYear: function() {
									$.datepicker._selectMonthYear( id, this, "Y" );
									return false;
								}
							};
							$( this ).on( this.getAttribute( "data-event" ), handler[ this.getAttribute( "data-handler" ) ] );
						} );
					},

					/* Generate the HTML for the current state of the date picker. */
					_generateHTML: function( inst ) {
						var maxDraw, prevText, prev, nextText, next, currentText, gotoDate,
							controls, buttonPanel, firstDay, showWeek, dayNames, dayNamesMin,
							monthNames, monthNamesShort, beforeShowDay, showOtherMonths,
							selectOtherMonths, defaultDate, html, dow, row, group, col, selectedDate,
							cornerClass, calender, thead, day, daysInMonth, leadDays, curRows, numRows,
							printDate, dRow, tbody, daySettings, otherMonth, unselectable,
							tempDate = new Date(),
							today = this._daylightSavingAdjust(
								new Date( tempDate.getFullYear(), tempDate.getMonth(), tempDate.getDate() ) ), // clear time
							isRTL = this._get( inst, "isRTL" ),
							showButtonPanel = this._get( inst, "showButtonPanel" ),
							hideIfNoPrevNext = this._get( inst, "hideIfNoPrevNext" ),
							navigationAsDateFormat = this._get( inst, "navigationAsDateFormat" ),
							numMonths = this._getNumberOfMonths( inst ),
							showCurrentAtPos = this._get( inst, "showCurrentAtPos" ),
							stepMonths = this._get( inst, "stepMonths" ),
							isMultiMonth = ( numMonths[ 0 ] !== 1 || numMonths[ 1 ] !== 1 ),
							currentDate = this._daylightSavingAdjust( ( !inst.currentDay ? new Date( 9999, 9, 9 ) :
								new Date( inst.currentYear, inst.currentMonth, inst.currentDay ) ) ),
							minDate = this._getMinMaxDate( inst, "min" ),
							maxDate = this._getMinMaxDate( inst, "max" ),
							drawMonth = inst.drawMonth - showCurrentAtPos,
							drawYear = inst.drawYear;

						if ( drawMonth < 0 ) {
							drawMonth += 12;
							drawYear--;
						}
						if ( maxDate ) {
							maxDraw = this._daylightSavingAdjust( new Date( maxDate.getFullYear(),
								maxDate.getMonth() - ( numMonths[ 0 ] * numMonths[ 1 ] ) + 1, maxDate.getDate() ) );
							maxDraw = ( minDate && maxDraw < minDate ? minDate : maxDraw );
							while ( this._daylightSavingAdjust( new Date( drawYear, drawMonth, 1 ) ) > maxDraw ) {
								drawMonth--;
								if ( drawMonth < 0 ) {
									drawMonth = 11;
									drawYear--;
								}
							}
						}
						inst.drawMonth = drawMonth;
						inst.drawYear = drawYear;

						prevText = this._get( inst, "prevText" );
						prevText = ( !navigationAsDateFormat ? prevText : this.formatDate( prevText,
							this._daylightSavingAdjust( new Date( drawYear, drawMonth - stepMonths, 1 ) ),
							this._getFormatConfig( inst ) ) );

						prev = ( this._canAdjustMonth( inst, -1, drawYear, drawMonth ) ?
							"<a class='ui-datepicker-prev ui-corner-all' data-handler='prev' data-event='click'" +
							" title='" + prevText + "'><span class='ui-icon ui-icon-circle-triangle-" + ( isRTL ? "e" : "w" ) + "'>" + prevText + "</span></a>" :
							( hideIfNoPrevNext ? "" : "<a class='ui-datepicker-prev ui-corner-all ui-state-disabled' title='" + prevText + "'><span class='ui-icon ui-icon-circle-triangle-" + ( isRTL ? "e" : "w" ) + "'>" + prevText + "</span></a>" ) );

						nextText = this._get( inst, "nextText" );
						nextText = ( !navigationAsDateFormat ? nextText : this.formatDate( nextText,
							this._daylightSavingAdjust( new Date( drawYear, drawMonth + stepMonths, 1 ) ),
							this._getFormatConfig( inst ) ) );

						next = ( this._canAdjustMonth( inst, +1, drawYear, drawMonth ) ?
							"<a class='ui-datepicker-next ui-corner-all' data-handler='next' data-event='click'" +
							" title='" + nextText + "'><span class='ui-icon ui-icon-circle-triangle-" + ( isRTL ? "w" : "e" ) + "'>" + nextText + "</span></a>" :
							( hideIfNoPrevNext ? "" : "<a class='ui-datepicker-next ui-corner-all ui-state-disabled' title='" + nextText + "'><span class='ui-icon ui-icon-circle-triangle-" + ( isRTL ? "w" : "e" ) + "'>" + nextText + "</span></a>" ) );

						currentText = this._get( inst, "currentText" );
						gotoDate = ( this._get( inst, "gotoCurrent" ) && inst.currentDay ? currentDate : today );
						currentText = ( !navigationAsDateFormat ? currentText :
							this.formatDate( currentText, gotoDate, this._getFormatConfig( inst ) ) );

						controls = ( !inst.inline ? "<button type='button' class='ui-datepicker-close ui-state-default ui-priority-primary ui-corner-all' data-handler='hide' data-event='click'>" +
							this._get( inst, "closeText" ) + "</button>" : "" );

						buttonPanel = ( showButtonPanel ) ? "<div class='ui-datepicker-buttonpane ui-widget-content'>" + ( isRTL ? controls : "" ) +
							( this._isInRange( inst, gotoDate ) ? "<button type='button' class='ui-datepicker-current ui-state-default ui-priority-secondary ui-corner-all' data-handler='today' data-event='click'" +
								">" + currentText + "</button>" : "" ) + ( isRTL ? "" : controls ) + "</div>" : "";

						firstDay = parseInt( this._get( inst, "firstDay" ), 10 );
						firstDay = ( isNaN( firstDay ) ? 0 : firstDay );

						showWeek = this._get( inst, "showWeek" );
						dayNames = this._get( inst, "dayNames" );
						dayNamesMin = this._get( inst, "dayNamesMin" );
						monthNames = this._get( inst, "monthNames" );
						monthNamesShort = this._get( inst, "monthNamesShort" );
						beforeShowDay = this._get( inst, "beforeShowDay" );
						showOtherMonths = this._get( inst, "showOtherMonths" );
						selectOtherMonths = this._get( inst, "selectOtherMonths" );
						defaultDate = this._getDefaultDate( inst );
						html = "";

						for ( row = 0; row < numMonths[ 0 ]; row++ ) {
							group = "";
							this.maxRows = 4;
							for ( col = 0; col < numMonths[ 1 ]; col++ ) {
								selectedDate = this._daylightSavingAdjust( new Date( drawYear, drawMonth, inst.selectedDay ) );
								cornerClass = " ui-corner-all";
								calender = "";
								if ( isMultiMonth ) {
									calender += "<div class='ui-datepicker-group";
									if ( numMonths[ 1 ] > 1 ) {
										switch ( col ) {
											case 0: calender += " ui-datepicker-group-first";
												cornerClass = " ui-corner-" + ( isRTL ? "right" : "left" ); break;
											case numMonths[ 1 ] - 1: calender += " ui-datepicker-group-last";
												cornerClass = " ui-corner-" + ( isRTL ? "left" : "right" ); break;
											default: calender += " ui-datepicker-group-middle"; cornerClass = ""; break;
										}
									}
									calender += "'>";
								}
								calender += "<div class='ui-datepicker-header ui-widget-header ui-helper-clearfix" + cornerClass + "'>" +
									( /all|left/.test( cornerClass ) && row === 0 ? ( isRTL ? next : prev ) : "" ) +
									( /all|right/.test( cornerClass ) && row === 0 ? ( isRTL ? prev : next ) : "" ) +
									this._generateMonthYearHeader( inst, drawMonth, drawYear, minDate, maxDate,
										row > 0 || col > 0, monthNames, monthNamesShort ) + // draw month headers
									"</div><table class='ui-datepicker-calendar'><thead>" +
									"<tr>";
								thead = ( showWeek ? "<th class='ui-datepicker-week-col'>" + this._get( inst, "weekHeader" ) + "</th>" : "" );
								for ( dow = 0; dow < 7; dow++ ) { // days of the week
									day = ( dow + firstDay ) % 7;
									thead += "<th scope='col'" + ( ( dow + firstDay + 6 ) % 7 >= 5 ? " class='ui-datepicker-week-end'" : "" ) + ">" +
										"<span title='" + dayNames[ day ] + "'>" + dayNamesMin[ day ] + "</span></th>";
								}
								calender += thead + "</tr></thead><tbody>";
								daysInMonth = this._getDaysInMonth( drawYear, drawMonth );
								if ( drawYear === inst.selectedYear && drawMonth === inst.selectedMonth ) {
									inst.selectedDay = Math.min( inst.selectedDay, daysInMonth );
								}
								leadDays = ( this._getFirstDayOfMonth( drawYear, drawMonth ) - firstDay + 7 ) % 7;
								curRows = Math.ceil( ( leadDays + daysInMonth ) / 7 ); // calculate the number of rows to generate
								numRows = ( isMultiMonth ? this.maxRows > curRows ? this.maxRows : curRows : curRows ); //If multiple months, use the higher number of rows (see #7043)
								this.maxRows = numRows;
								printDate = this._daylightSavingAdjust( new Date( drawYear, drawMonth, 1 - leadDays ) );
								for ( dRow = 0; dRow < numRows; dRow++ ) { // create date picker rows
									calender += "<tr>";
									tbody = ( !showWeek ? "" : "<td class='ui-datepicker-week-col'>" +
										this._get( inst, "calculateWeek" )( printDate ) + "</td>" );
									for ( dow = 0; dow < 7; dow++ ) { // create date picker days
										daySettings = ( beforeShowDay ?
											beforeShowDay.apply( ( inst.input ? inst.input[ 0 ] : null ), [ printDate ] ) : [ true, "" ] );
										otherMonth = ( printDate.getMonth() !== drawMonth );
										unselectable = ( otherMonth && !selectOtherMonths ) || !daySettings[ 0 ] ||
											( minDate && printDate < minDate ) || ( maxDate && printDate > maxDate );
										tbody += "<td class='" +
											( ( dow + firstDay + 6 ) % 7 >= 5 ? " ui-datepicker-week-end" : "" ) + // highlight weekends
											( otherMonth ? " ui-datepicker-other-month" : "" ) + // highlight days from other months
											( ( printDate.getTime() === selectedDate.getTime() && drawMonth === inst.selectedMonth && inst._keyEvent ) || // user pressed key
											( defaultDate.getTime() === printDate.getTime() && defaultDate.getTime() === selectedDate.getTime() ) ?

												// or defaultDate is current printedDate and defaultDate is selectedDate
												" " + this._dayOverClass : "" ) + // highlight selected day
											( unselectable ? " " + this._unselectableClass + " ui-state-disabled" : "" ) +  // highlight unselectable days
											( otherMonth && !showOtherMonths ? "" : " " + daySettings[ 1 ] + // highlight custom dates
												( printDate.getTime() === currentDate.getTime() ? " " + this._currentClass : "" ) + // highlight selected day
												( printDate.getTime() === today.getTime() ? " ui-datepicker-today" : "" ) ) + "'" + // highlight today (if different)
											( ( !otherMonth || showOtherMonths ) && daySettings[ 2 ] ? " title='" + daySettings[ 2 ].replace( /'/g, "&#39;" ) + "'" : "" ) + // cell title
											( unselectable ? "" : " data-handler='selectDay' data-event='click' data-month='" + printDate.getMonth() + "' data-year='" + printDate.getFullYear() + "'" ) + ">" + // actions
											( otherMonth && !showOtherMonths ? "&#xa0;" : // display for other months
												( unselectable ? "<span class='ui-state-default'>" + printDate.getDate() + "</span>" : "<a class='ui-state-default" +
													( printDate.getTime() === today.getTime() ? " ui-state-highlight" : "" ) +
													( printDate.getTime() === currentDate.getTime() ? " ui-state-active" : "" ) + // highlight selected day
													( otherMonth ? " ui-priority-secondary" : "" ) + // distinguish dates from other months
													"' href='#'>" + printDate.getDate() + "</a>" ) ) + "</td>"; // display selectable date
										printDate.setDate( printDate.getDate() + 1 );
										printDate = this._daylightSavingAdjust( printDate );
									}
									calender += tbody + "</tr>";
								}
								drawMonth++;
								if ( drawMonth > 11 ) {
									drawMonth = 0;
									drawYear++;
								}
								calender += "</tbody></table>" + ( isMultiMonth ? "</div>" +
									( ( numMonths[ 0 ] > 0 && col === numMonths[ 1 ] - 1 ) ? "<div class='ui-datepicker-row-break'></div>" : "" ) : "" );
								group += calender;
							}
							html += group;
						}
						html += buttonPanel;
						inst._keyEvent = false;
						return html;
					},

					/* Generate the month and year header. */
					_generateMonthYearHeader: function( inst, drawMonth, drawYear, minDate, maxDate,
														secondary, monthNames, monthNamesShort ) {

						var inMinYear, inMaxYear, month, years, thisYear, determineYear, year, endYear,
							changeMonth = this._get( inst, "changeMonth" ),
							changeYear = this._get( inst, "changeYear" ),
							showMonthAfterYear = this._get( inst, "showMonthAfterYear" ),
							html = "<div class='ui-datepicker-title'>",
							monthHtml = "";

						// Month selection
						if ( secondary || !changeMonth ) {
							monthHtml += "<span class='ui-datepicker-month'>" + monthNames[ drawMonth ] + "</span>";
						} else {
							inMinYear = ( minDate && minDate.getFullYear() === drawYear );
							inMaxYear = ( maxDate && maxDate.getFullYear() === drawYear );
							monthHtml += "<select class='ui-datepicker-month' data-handler='selectMonth' data-event='change'>";
							for ( month = 0; month < 12; month++ ) {
								if ( ( !inMinYear || month >= minDate.getMonth() ) && ( !inMaxYear || month <= maxDate.getMonth() ) ) {
									monthHtml += "<option value='" + month + "'" +
										( month === drawMonth ? " selected='selected'" : "" ) +
										">" + monthNamesShort[ month ] + "</option>";
								}
							}
							monthHtml += "</select>";
						}

						if ( !showMonthAfterYear ) {
							html += monthHtml + ( secondary || !( changeMonth && changeYear ) ? "&#xa0;" : "" );
						}

						// Year selection
						if ( !inst.yearshtml ) {
							inst.yearshtml = "";
							if ( secondary || !changeYear ) {
								html += "<span class='ui-datepicker-year'>" + drawYear + "</span>";
							} else {

								// determine range of years to display
								years = this._get( inst, "yearRange" ).split( ":" );
								thisYear = new Date().getFullYear();
								determineYear = function( value ) {
									var year = ( value.match( /c[+\-].*/ ) ? drawYear + parseInt( value.substring( 1 ), 10 ) :
										( value.match( /[+\-].*/ ) ? thisYear + parseInt( value, 10 ) :
											parseInt( value, 10 ) ) );
									return ( isNaN( year ) ? thisYear : year );
								};
								year = determineYear( years[ 0 ] );
								endYear = Math.max( year, determineYear( years[ 1 ] || "" ) );
								year = ( minDate ? Math.max( year, minDate.getFullYear() ) : year );
								endYear = ( maxDate ? Math.min( endYear, maxDate.getFullYear() ) : endYear );
								inst.yearshtml += "<select class='ui-datepicker-year' data-handler='selectYear' data-event='change'>";
								for ( ; year <= endYear; year++ ) {
									inst.yearshtml += "<option value='" + year + "'" +
										( year === drawYear ? " selected='selected'" : "" ) +
										">" + year + "</option>";
								}
								inst.yearshtml += "</select>";

								html += inst.yearshtml;
								inst.yearshtml = null;
							}
						}

						html += this._get( inst, "yearSuffix" );
						if ( showMonthAfterYear ) {
							html += ( secondary || !( changeMonth && changeYear ) ? "&#xa0;" : "" ) + monthHtml;
						}
						html += "</div>"; // Close datepicker_header
						return html;
					},

					/* Adjust one of the date sub-fields. */
					_adjustInstDate: function( inst, offset, period ) {
						var year = inst.selectedYear + ( period === "Y" ? offset : 0 ),
							month = inst.selectedMonth + ( period === "M" ? offset : 0 ),
							day = Math.min( inst.selectedDay, this._getDaysInMonth( year, month ) ) + ( period === "D" ? offset : 0 ),
							date = this._restrictMinMax( inst, this._daylightSavingAdjust( new Date( year, month, day ) ) );

						inst.selectedDay = date.getDate();
						inst.drawMonth = inst.selectedMonth = date.getMonth();
						inst.drawYear = inst.selectedYear = date.getFullYear();
						if ( period === "M" || period === "Y" ) {
							this._notifyChange( inst );
						}
					},

					/* Ensure a date is within any min/max bounds. */
					_restrictMinMax: function( inst, date ) {
						var minDate = this._getMinMaxDate( inst, "min" ),
							maxDate = this._getMinMaxDate( inst, "max" ),
							newDate = ( minDate && date < minDate ? minDate : date );
						return ( maxDate && newDate > maxDate ? maxDate : newDate );
					},

					/* Notify change of month/year. */
					_notifyChange: function( inst ) {
						var onChange = this._get( inst, "onChangeMonthYear" );
						if ( onChange ) {
							onChange.apply( ( inst.input ? inst.input[ 0 ] : null ),
								[ inst.selectedYear, inst.selectedMonth + 1, inst ] );
						}
					},

					/* Determine the number of months to show. */
					_getNumberOfMonths: function( inst ) {
						var numMonths = this._get( inst, "numberOfMonths" );
						return ( numMonths == null ? [ 1, 1 ] : ( typeof numMonths === "number" ? [ 1, numMonths ] : numMonths ) );
					},

					/* Determine the current maximum date - ensure no time components are set. */
					_getMinMaxDate: function( inst, minMax ) {
						return this._determineDate( inst, this._get( inst, minMax + "Date" ), null );
					},

					/* Find the number of days in a given month. */
					_getDaysInMonth: function( year, month ) {
						return 32 - this._daylightSavingAdjust( new Date( year, month, 32 ) ).getDate();
					},

					/* Find the day of the week of the first of a month. */
					_getFirstDayOfMonth: function( year, month ) {
						return new Date( year, month, 1 ).getDay();
					},

					/* Determines if we should allow a "next/prev" month display change. */
					_canAdjustMonth: function( inst, offset, curYear, curMonth ) {
						var numMonths = this._getNumberOfMonths( inst ),
							date = this._daylightSavingAdjust( new Date( curYear,
								curMonth + ( offset < 0 ? offset : numMonths[ 0 ] * numMonths[ 1 ] ), 1 ) );

						if ( offset < 0 ) {
							date.setDate( this._getDaysInMonth( date.getFullYear(), date.getMonth() ) );
						}
						return this._isInRange( inst, date );
					},

					/* Is the given date in the accepted range? */
					_isInRange: function( inst, date ) {
						var yearSplit, currentYear,
							minDate = this._getMinMaxDate( inst, "min" ),
							maxDate = this._getMinMaxDate( inst, "max" ),
							minYear = null,
							maxYear = null,
							years = this._get( inst, "yearRange" );
						if ( years ) {
							yearSplit = years.split( ":" );
							currentYear = new Date().getFullYear();
							minYear = parseInt( yearSplit[ 0 ], 10 );
							maxYear = parseInt( yearSplit[ 1 ], 10 );
							if ( yearSplit[ 0 ].match( /[+\-].*/ ) ) {
								minYear += currentYear;
							}
							if ( yearSplit[ 1 ].match( /[+\-].*/ ) ) {
								maxYear += currentYear;
							}
						}

						return ( ( !minDate || date.getTime() >= minDate.getTime() ) &&
							( !maxDate || date.getTime() <= maxDate.getTime() ) &&
							( !minYear || date.getFullYear() >= minYear ) &&
							( !maxYear || date.getFullYear() <= maxYear ) );
					},

					/* Provide the configuration settings for formatting/parsing. */
					_getFormatConfig: function( inst ) {
						var shortYearCutoff = this._get( inst, "shortYearCutoff" );
						shortYearCutoff = ( typeof shortYearCutoff !== "string" ? shortYearCutoff :
							new Date().getFullYear() % 100 + parseInt( shortYearCutoff, 10 ) );
						return { shortYearCutoff: shortYearCutoff,
							dayNamesShort: this._get( inst, "dayNamesShort" ), dayNames: this._get( inst, "dayNames" ),
							monthNamesShort: this._get( inst, "monthNamesShort" ), monthNames: this._get( inst, "monthNames" ) };
					},

					/* Format the given date for display. */
					_formatDate: function( inst, day, month, year ) {
						if ( !day ) {
							inst.currentDay = inst.selectedDay;
							inst.currentMonth = inst.selectedMonth;
							inst.currentYear = inst.selectedYear;
						}
						var date = ( day ? ( typeof day === "object" ? day :
							this._daylightSavingAdjust( new Date( year, month, day ) ) ) :
							this._daylightSavingAdjust( new Date( inst.currentYear, inst.currentMonth, inst.currentDay ) ) );
						return this.formatDate( this._get( inst, "dateFormat" ), date, this._getFormatConfig( inst ) );
					}
				} );

				/*
 * Bind hover events for datepicker elements.
 * Done via delegate so the binding only occurs once in the lifetime of the parent div.
 * Global datepicker_instActive, set by _updateDatepicker allows the handlers to find their way back to the active picker.
 */
				function datepicker_bindHover( dpDiv ) {
					var selector = "button, .ui-datepicker-prev, .ui-datepicker-next, .ui-datepicker-calendar td a";
					return dpDiv.on( "mouseout", selector, function() {
						$( this ).removeClass( "ui-state-hover" );
						if ( this.className.indexOf( "ui-datepicker-prev" ) !== -1 ) {
							$( this ).removeClass( "ui-datepicker-prev-hover" );
						}
						if ( this.className.indexOf( "ui-datepicker-next" ) !== -1 ) {
							$( this ).removeClass( "ui-datepicker-next-hover" );
						}
					} )
						.on( "mouseover", selector, datepicker_handleMouseover );
				}

				function datepicker_handleMouseover() {
					if ( !$.datepicker._isDisabledDatepicker( datepicker_instActive.inline ? datepicker_instActive.dpDiv.parent()[ 0 ] : datepicker_instActive.input[ 0 ] ) ) {
						$( this ).parents( ".ui-datepicker-calendar" ).find( "a" ).removeClass( "ui-state-hover" );
						$( this ).addClass( "ui-state-hover" );
						if ( this.className.indexOf( "ui-datepicker-prev" ) !== -1 ) {
							$( this ).addClass( "ui-datepicker-prev-hover" );
						}
						if ( this.className.indexOf( "ui-datepicker-next" ) !== -1 ) {
							$( this ).addClass( "ui-datepicker-next-hover" );
						}
					}
				}

				/* jQuery extend now ignores nulls! */
				function datepicker_extendRemove( target, props ) {
					$.extend( target, props );
					for ( var name in props ) {
						if ( props[ name ] == null ) {
							target[ name ] = props[ name ];
						}
					}
					return target;
				}

				/* Invoke the datepicker functionality.
   @param  options  string - a command, optionally followed by additional parameters or
					Object - settings for attaching new datepicker functionality
   @return  jQuery object */
				$.fn.datepicker = function( options ) {

					/* Verify an empty collection wasn't passed - Fixes #6976 */
					if ( !this.length ) {
						return this;
					}

					/* Initialise the date picker. */
					if ( !$.datepicker.initialized ) {
						$( document ).on( "mousedown", $.datepicker._checkExternalClick );
						$.datepicker.initialized = true;
					}

					/* Append datepicker main container to body if not exist. */
					if ( $( "#" + $.datepicker._mainDivId ).length === 0 ) {
						$( "body" ).append( $.datepicker.dpDiv );
					}

					var otherArgs = Array.prototype.slice.call( arguments, 1 );
					if ( typeof options === "string" && ( options === "isDisabled" || options === "getDate" || options === "widget" ) ) {
						return $.datepicker[ "_" + options + "Datepicker" ].
						apply( $.datepicker, [ this[ 0 ] ].concat( otherArgs ) );
					}
					if ( options === "option" && arguments.length === 2 && typeof arguments[ 1 ] === "string" ) {
						return $.datepicker[ "_" + options + "Datepicker" ].
						apply( $.datepicker, [ this[ 0 ] ].concat( otherArgs ) );
					}
					return this.each( function() {
						typeof options === "string" ?
							$.datepicker[ "_" + options + "Datepicker" ].
							apply( $.datepicker, [ this ].concat( otherArgs ) ) :
							$.datepicker._attachDatepicker( this, options );
					} );
				};

				$.datepicker = new Datepicker(); // singleton instance
				$.datepicker.initialized = false;
				$.datepicker.uuid = new Date().getTime();
				$.datepicker.version = "1.12.1";

				return $.datepicker;

			} ) );


			/***/ }),

		/***/ "../node_modules/wowjs/dist/wow.js":
		/*!*****************************************!*\
  !*** ../node_modules/wowjs/dist/wow.js ***!
  \*****************************************/
		/*! no static exports found */
		/***/ (function(module, exports) {

			(function() {
				var MutationObserver, Util, WeakMap, getComputedStyle, getComputedStyleRX,
					bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
					indexOf = [].indexOf || function(item) { for (var i = 0, l = this.length; i < l; i++) { if (i in this && this[i] === item) return i; } return -1; };

				Util = (function() {
					function Util() {}

					Util.prototype.extend = function(custom, defaults) {
						var key, value;
						for (key in defaults) {
							value = defaults[key];
							if (custom[key] == null) {
								custom[key] = value;
							}
						}
						return custom;
					};

					Util.prototype.isMobile = function(agent) {
						return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(agent);
					};

					Util.prototype.createEvent = function(event, bubble, cancel, detail) {
						var customEvent;
						if (bubble == null) {
							bubble = false;
						}
						if (cancel == null) {
							cancel = false;
						}
						if (detail == null) {
							detail = null;
						}
						if (document.createEvent != null) {
							customEvent = document.createEvent('CustomEvent');
							customEvent.initCustomEvent(event, bubble, cancel, detail);
						} else if (document.createEventObject != null) {
							customEvent = document.createEventObject();
							customEvent.eventType = event;
						} else {
							customEvent.eventName = event;
						}
						return customEvent;
					};

					Util.prototype.emitEvent = function(elem, event) {
						if (elem.dispatchEvent != null) {
							return elem.dispatchEvent(event);
						} else if (event in (elem != null)) {
							return elem[event]();
						} else if (("on" + event) in (elem != null)) {
							return elem["on" + event]();
						}
					};

					Util.prototype.addEvent = function(elem, event, fn) {
						if (elem.addEventListener != null) {
							return elem.addEventListener(event, fn, false);
						} else if (elem.attachEvent != null) {
							return elem.attachEvent("on" + event, fn);
						} else {
							return elem[event] = fn;
						}
					};

					Util.prototype.removeEvent = function(elem, event, fn) {
						if (elem.removeEventListener != null) {
							return elem.removeEventListener(event, fn, false);
						} else if (elem.detachEvent != null) {
							return elem.detachEvent("on" + event, fn);
						} else {
							return delete elem[event];
						}
					};

					Util.prototype.innerHeight = function() {
						if ('innerHeight' in window) {
							return window.innerHeight;
						} else {
							return document.documentElement.clientHeight;
						}
					};

					return Util;

				})();

				WeakMap = this.WeakMap || this.MozWeakMap || (WeakMap = (function() {
					function WeakMap() {
						this.keys = [];
						this.values = [];
					}

					WeakMap.prototype.get = function(key) {
						var i, item, j, len, ref;
						ref = this.keys;
						for (i = j = 0, len = ref.length; j < len; i = ++j) {
							item = ref[i];
							if (item === key) {
								return this.values[i];
							}
						}
					};

					WeakMap.prototype.set = function(key, value) {
						var i, item, j, len, ref;
						ref = this.keys;
						for (i = j = 0, len = ref.length; j < len; i = ++j) {
							item = ref[i];
							if (item === key) {
								this.values[i] = value;
								return;
							}
						}
						this.keys.push(key);
						return this.values.push(value);
					};

					return WeakMap;

				})());

				MutationObserver = this.MutationObserver || this.WebkitMutationObserver || this.MozMutationObserver || (MutationObserver = (function() {
					function MutationObserver() {
						if (typeof console !== "undefined" && console !== null) {
							console.warn('MutationObserver is not supported by your browser.');
						}
						if (typeof console !== "undefined" && console !== null) {
							console.warn('WOW.js cannot detect dom mutations, please call .sync() after loading new content.');
						}
					}

					MutationObserver.notSupported = true;

					MutationObserver.prototype.observe = function() {};

					return MutationObserver;

				})());

				getComputedStyle = this.getComputedStyle || function(el, pseudo) {
					this.getPropertyValue = function(prop) {
						var ref;
						if (prop === 'float') {
							prop = 'styleFloat';
						}
						if (getComputedStyleRX.test(prop)) {
							prop.replace(getComputedStyleRX, function(_, _char) {
								return _char.toUpperCase();
							});
						}
						return ((ref = el.currentStyle) != null ? ref[prop] : void 0) || null;
					};
					return this;
				};

				getComputedStyleRX = /(\-([a-z]){1})/g;

				this.WOW = (function() {
					WOW.prototype.defaults = {
						boxClass: 'wow',
						animateClass: 'animated',
						offset: 0,
						mobile: true,
						live: true,
						callback: null,
						scrollContainer: null
					};

					function WOW(options) {
						if (options == null) {
							options = {};
						}
						this.scrollCallback = bind(this.scrollCallback, this);
						this.scrollHandler = bind(this.scrollHandler, this);
						this.resetAnimation = bind(this.resetAnimation, this);
						this.start = bind(this.start, this);
						this.scrolled = true;
						this.config = this.util().extend(options, this.defaults);
						if (options.scrollContainer != null) {
							this.config.scrollContainer = document.querySelector(options.scrollContainer);
						}
						this.animationNameCache = new WeakMap();
						this.wowEvent = this.util().createEvent(this.config.boxClass);
					}

					WOW.prototype.init = function() {
						var ref;
						this.element = window.document.documentElement;
						if ((ref = document.readyState) === "interactive" || ref === "complete") {
							this.start();
						} else {
							this.util().addEvent(document, 'DOMContentLoaded', this.start);
						}
						return this.finished = [];
					};

					WOW.prototype.start = function() {
						var box, j, len, ref;
						this.stopped = false;
						this.boxes = (function() {
							var j, len, ref, results;
							ref = this.element.querySelectorAll("." + this.config.boxClass);
							results = [];
							for (j = 0, len = ref.length; j < len; j++) {
								box = ref[j];
								results.push(box);
							}
							return results;
						}).call(this);
						this.all = (function() {
							var j, len, ref, results;
							ref = this.boxes;
							results = [];
							for (j = 0, len = ref.length; j < len; j++) {
								box = ref[j];
								results.push(box);
							}
							return results;
						}).call(this);
						if (this.boxes.length) {
							if (this.disabled()) {
								this.resetStyle();
							} else {
								ref = this.boxes;
								for (j = 0, len = ref.length; j < len; j++) {
									box = ref[j];
									this.applyStyle(box, true);
								}
							}
						}
						if (!this.disabled()) {
							this.util().addEvent(this.config.scrollContainer || window, 'scroll', this.scrollHandler);
							this.util().addEvent(window, 'resize', this.scrollHandler);
							this.interval = setInterval(this.scrollCallback, 50);
						}
						if (this.config.live) {
							return new MutationObserver((function(_this) {
								return function(records) {
									var k, len1, node, record, results;
									results = [];
									for (k = 0, len1 = records.length; k < len1; k++) {
										record = records[k];
										results.push((function() {
											var l, len2, ref1, results1;
											ref1 = record.addedNodes || [];
											results1 = [];
											for (l = 0, len2 = ref1.length; l < len2; l++) {
												node = ref1[l];
												results1.push(this.doSync(node));
											}
											return results1;
										}).call(_this));
									}
									return results;
								};
							})(this)).observe(document.body, {
								childList: true,
								subtree: true
							});
						}
					};

					WOW.prototype.stop = function() {
						this.stopped = true;
						this.util().removeEvent(this.config.scrollContainer || window, 'scroll', this.scrollHandler);
						this.util().removeEvent(window, 'resize', this.scrollHandler);
						if (this.interval != null) {
							return clearInterval(this.interval);
						}
					};

					WOW.prototype.sync = function(element) {
						if (MutationObserver.notSupported) {
							return this.doSync(this.element);
						}
					};

					WOW.prototype.doSync = function(element) {
						var box, j, len, ref, results;
						if (element == null) {
							element = this.element;
						}
						if (element.nodeType !== 1) {
							return;
						}
						element = element.parentNode || element;
						ref = element.querySelectorAll("." + this.config.boxClass);
						results = [];
						for (j = 0, len = ref.length; j < len; j++) {
							box = ref[j];
							if (indexOf.call(this.all, box) < 0) {
								this.boxes.push(box);
								this.all.push(box);
								if (this.stopped || this.disabled()) {
									this.resetStyle();
								} else {
									this.applyStyle(box, true);
								}
								results.push(this.scrolled = true);
							} else {
								results.push(void 0);
							}
						}
						return results;
					};

					WOW.prototype.show = function(box) {
						this.applyStyle(box);
						box.className = box.className + " " + this.config.animateClass;
						if (this.config.callback != null) {
							this.config.callback(box);
						}
						this.util().emitEvent(box, this.wowEvent);
						this.util().addEvent(box, 'animationend', this.resetAnimation);
						this.util().addEvent(box, 'oanimationend', this.resetAnimation);
						this.util().addEvent(box, 'webkitAnimationEnd', this.resetAnimation);
						this.util().addEvent(box, 'MSAnimationEnd', this.resetAnimation);
						return box;
					};

					WOW.prototype.applyStyle = function(box, hidden) {
						var delay, duration, iteration;
						duration = box.getAttribute('data-wow-duration');
						delay = box.getAttribute('data-wow-delay');
						iteration = box.getAttribute('data-wow-iteration');
						return this.animate((function(_this) {
							return function() {
								return _this.customStyle(box, hidden, duration, delay, iteration);
							};
						})(this));
					};

					WOW.prototype.animate = (function() {
						if ('requestAnimationFrame' in window) {
							return function(callback) {
								return window.requestAnimationFrame(callback);
							};
						} else {
							return function(callback) {
								return callback();
							};
						}
					})();

					WOW.prototype.resetStyle = function() {
						var box, j, len, ref, results;
						ref = this.boxes;
						results = [];
						for (j = 0, len = ref.length; j < len; j++) {
							box = ref[j];
							results.push(box.style.visibility = 'visible');
						}
						return results;
					};

					WOW.prototype.resetAnimation = function(event) {
						var target;
						if (event.type.toLowerCase().indexOf('animationend') >= 0) {
							target = event.target || event.srcElement;
							return target.className = target.className.replace(this.config.animateClass, '').trim();
						}
					};

					WOW.prototype.customStyle = function(box, hidden, duration, delay, iteration) {
						if (hidden) {
							this.cacheAnimationName(box);
						}
						box.style.visibility = hidden ? 'hidden' : 'visible';
						if (duration) {
							this.vendorSet(box.style, {
								animationDuration: duration
							});
						}
						if (delay) {
							this.vendorSet(box.style, {
								animationDelay: delay
							});
						}
						if (iteration) {
							this.vendorSet(box.style, {
								animationIterationCount: iteration
							});
						}
						this.vendorSet(box.style, {
							animationName: hidden ? 'none' : this.cachedAnimationName(box)
						});
						return box;
					};

					WOW.prototype.vendors = ["moz", "webkit"];

					WOW.prototype.vendorSet = function(elem, properties) {
						var name, results, value, vendor;
						results = [];
						for (name in properties) {
							value = properties[name];
							elem["" + name] = value;
							results.push((function() {
								var j, len, ref, results1;
								ref = this.vendors;
								results1 = [];
								for (j = 0, len = ref.length; j < len; j++) {
									vendor = ref[j];
									results1.push(elem["" + vendor + (name.charAt(0).toUpperCase()) + (name.substr(1))] = value);
								}
								return results1;
							}).call(this));
						}
						return results;
					};

					WOW.prototype.vendorCSS = function(elem, property) {
						var j, len, ref, result, style, vendor;
						style = getComputedStyle(elem);
						result = style.getPropertyCSSValue(property);
						ref = this.vendors;
						for (j = 0, len = ref.length; j < len; j++) {
							vendor = ref[j];
							result = result || style.getPropertyCSSValue("-" + vendor + "-" + property);
						}
						return result;
					};

					WOW.prototype.animationName = function(box) {
						var animationName, error;
						try {
							animationName = this.vendorCSS(box, 'animation-name').cssText;
						} catch (error) {
							animationName = getComputedStyle(box).getPropertyValue('animation-name');
						}
						if (animationName === 'none') {
							return '';
						} else {
							return animationName;
						}
					};

					WOW.prototype.cacheAnimationName = function(box) {
						return this.animationNameCache.set(box, this.animationName(box));
					};

					WOW.prototype.cachedAnimationName = function(box) {
						return this.animationNameCache.get(box);
					};

					WOW.prototype.scrollHandler = function() {
						return this.scrolled = true;
					};

					WOW.prototype.scrollCallback = function() {
						var box;
						if (this.scrolled) {
							this.scrolled = false;
							this.boxes = (function() {
								var j, len, ref, results;
								ref = this.boxes;
								results = [];
								for (j = 0, len = ref.length; j < len; j++) {
									box = ref[j];
									if (!(box)) {
										continue;
									}
									if (this.isVisible(box)) {
										this.show(box);
										continue;
									}
									results.push(box);
								}
								return results;
							}).call(this);
							if (!(this.boxes.length || this.config.live)) {
								return this.stop();
							}
						}
					};

					WOW.prototype.offsetTop = function(element) {
						var top;
						while (element.offsetTop === void 0) {
							element = element.parentNode;
						}
						top = element.offsetTop;
						while (element = element.offsetParent) {
							top += element.offsetTop;
						}
						return top;
					};

					WOW.prototype.isVisible = function(box) {
						var bottom, offset, top, viewBottom, viewTop;
						offset = box.getAttribute('data-wow-offset') || this.config.offset;
						viewTop = (this.config.scrollContainer && this.config.scrollContainer.scrollTop) || window.pageYOffset;
						viewBottom = viewTop + Math.min(this.element.clientHeight, this.util().innerHeight()) - offset;
						top = this.offsetTop(box);
						bottom = top + box.clientHeight;
						return top <= viewBottom && bottom >= viewTop;
					};

					WOW.prototype.util = function() {
						return this._util != null ? this._util : this._util = new Util();
					};

					WOW.prototype.disabled = function() {
						return !this.config.mobile && this.util().isMobile(navigator.userAgent);
					};

					return WOW;

				})();

			}).call(this);


			/***/ }),

		/***/ "./scripts/main.js":
		/*!*************************!*\
  !*** ./scripts/main.js ***!
  \*************************/
		/*! no exports provided */
		/***/ (function(module, __webpack_exports__, __webpack_require__) {

			"use strict";
			__webpack_require__.r(__webpack_exports__);
			/* WEBPACK VAR INJECTION */(function(jQuery) {/* harmony import */ var wowjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! wowjs */ "../node_modules/wowjs/dist/wow.js");
				/* harmony import */ var wowjs__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(wowjs__WEBPACK_IMPORTED_MODULE_0__);


				__webpack_require__(/*! jquery-ui/ui/core */ "../node_modules/jquery-ui/ui/core.js");
				__webpack_require__(/*! jquery-ui/ui/effect */ "../node_modules/jquery-ui/ui/effect.js");
				__webpack_require__(/*! jquery-ui/ui/widgets/datepicker */ "../node_modules/jquery-ui/ui/widgets/datepicker.js");

				/* eslint-disable-next-line */
				(function ($, ajax_localize) {

					$(document).ready(function () {

						var wow = new wowjs__WEBPACK_IMPORTED_MODULE_0__["WOW"]();

						wow.init();

						var headerScrollTopOffset = 0;

						if ($("body").hasClass("admin-bar") && ($(window).width() > 600)) {
							headerScrollTopOffset = $("html").css("margin-top");
						}

						// Watch for when header is scrolled to or away from the top of the viewport
						// If header is scrolled to the top of the page, we know it's been stickied based on it having position: sticky
						// Add class when it's sitting at the top of the page, remove it when it's been scrolled away
						$(".site-header-wrap").observeScrollTopChanges(headerScrollTopOffset).on("scroll-top-change", function (evt) {

							$(evt.target).toggleClass("sticky", evt.detail.scrolledToTop);

						});

						// Top level dropdown toggles
						//$(".header-navbar .navbar-nav").find(".menu-item-has-children").children(".dropdown-toggle").attr("data-toggle", "dropdown").attr("role","button");

						// Second level dropdown toggles
						$(".header-navbar .navbar-nav").find(".dropdown > .dropdown-menu > .dropdown").attr("data-toggle", "");
						/*$(".header-navbar .navbar-nav").find(".dropdown > .dropdown-menu > .dropdown").each(function () {

            let dropdownMenu = $(this).find(".dropdown-menu");

            // Briefly show parent dropdown so sub menu is visible
            $(this).closest(".dropdown-menu").css("display", "block");

            dropdownMenu.css({
                "display": "block",
                "position": "absolute",
                "left": "-100%",
                "max-height": "initial",
                "height": "auto",
            });

            var subMenuHeight = dropdownMenu.outerHeight();

            // Hide parent dropdown
            $(this).closest(".dropdown-menu").css("display", "");

            dropdownMenu
                .data("full-height", subMenuHeight)
                .css({
                    "display": "",
                    "position": "",
                    "left": "",
                    "max-height": "",
                    "height": "",
                });

            $(this).on("show.bs.dropdown", function () {
                dropdownMenu.css("height", dropdownMenu.data("full-height"));
            });

            $(this).on("hide.bs.dropdown", function () {
                dropdownMenu.css("height", "0");
            });

        });*/

						/*
            Mobile menu dropdown
         */

						if ($(window).width() < 992) {
							$('.navbar-collapse').find('.dropdown.menu-item-has-children').children('.nav-link').click(function (e) {
								e.preventDefault();
								$(this).siblings().toggleClass('menu-expanded');


								$('.close-list-item').click(function () {
									$(this).parent().removeClass('menu-expanded');
								})
							});


							$('.secondary-menu-wrap .menu-item-has-children .nav-link').click(function(e){
								e.preventDefault();

								$(this).parent().toggleClass('active');
							});



						}
						// console.log('running');
						//$('.menu-holiday-schedule .nav-link').prepend('<img class="logged-in-icon" src="'+document.location.origin+'/wp-content/themes/canpar/assets/images/icons/Calendar.svg"/>');
						// $('.menu-holiday-schedule .nav-link').append('<div>test</div>');

						if ($(window).width() < 992) {
							$('.dropdown-toggle').attr('data-toggle', '');
							$(".dropdown-menu").append('<li class="close-list-item">Back</li>');

							$('.dropdown-menu').each(function () {
								var title = $(this).siblings().text();
								var href = $(this).siblings().attr('href');

								$(this).prepend('<li class="list-heading"><a href="' + href + '">' + title + '</a></li>');
							});
						}

						$('.navbar-collapse')
							.on('hide.bs.collapse', function () {
								$('body').removeClass('no-scroll');
							})
							.on('show.bs.collapse', function (evt) {

								$('body').addClass('no-scroll');

								// Get position and size of header navbar element so we can calculate top position of mobile men
								var headerBoundingRect = $(".site-header-wrap").get(0).getBoundingClientRect();

								// Position top of mobile menu directly below header navbar
								$(evt.target).css("top", headerBoundingRect.bottom);

							});


						// // fbx-modal
						// // identify an element to observe
						// const elementToObserve = document.querySelector(".fbx-modal");
						//
						// // create a new instance of `MutationObserver` named `observer`,
						// // passing it a callback function
						// const observer = new MutationObserver(function() {
						//     console.log('callback that runs when observer is triggered');
						// });
						// observer.observe(elementToObserve);
						// // call `observe()` on that MutationObserver instance,
						// // passing it the element to observe, and the options object
						// //observer.observe(elementToObserve, {subtree: true, childList: true});


						$(".header-navbar .btn-search").on("click", function () {

							$("#header-search-overlay")
								.addClass("fade show")
								.find(".search-input")
								.focus();

							var headerPositionData = $(".site-header-wrap").get(0).getBoundingClientRect();

							$("<div>")
								.addClass("modal-backdrop header-search-overlay-backdrop")
								.css("opacity", 0) // Temporary
								.css("top", (headerPositionData.y + $("#header-search-overlay").outerHeight()))
								.appendTo($("body")).fadeTo(150, 0.2);

							$("body").addClass("modal-open");

						});

						$("#js-header-search-overlay-close").on("click", function () {

							$("#header-search-overlay")
								.removeClass("fade show")
								.find(".search-input")
								.blur();

							$(".header-search-overlay-backdrop").fadeOut(200, "linear", function () {
								$(this).remove();
							});

							$("body").removeClass("modal-open");

						});

						$(window).on("load resize orientationchange", normalizeOrangeCarouselHeights);


						// Datepicker form field initialization
						$(".date-picker-input").datepicker({
							dateFormat: "dd / mm / yy",
							changeMonth: true,
							changeYear: true,
						});

						$(".custom-file-input").on("change", function (evt) {

							var selectedFiles = $(evt.target).get(0).files;

							// Update text of the span element in neighbouring label element to the selected file name
							$(evt.target).siblings(".custom-file-label").children("span").text(selectedFiles[0].name);

						});

						$(".form-tooltip").tooltip({html: true});

						/**
						 * Alerts - Update "See Details" text on open/close
						 */
						$('.alert .btn-close').on('click load', function () {
							$('body.tfi-site-ics .site-header-wrap').addClass('alert-closed');
						});

						/**
						 * Alert Banner Slider - Add .active to first slide
						 */
						$('#alert-slider .carousel-inner .carousel-item:first-of-type').addClass('active');

						/**
						 * Alerts - scroll to alert when clicked in the banner
						 */
						$.each($('#alert-accordion .accordion-item-card'), function () {
							var id = $(this).find('.card-header').attr('id');
							var show = ($(this).find('.card-header .alert-header-trigger-btn .btn-accordion-trigger').attr('aria-expanded') === 'true');

							if (show) {
								$("html, body").animate({
									scrollTop: $('#' + id).offset().top - 200,
								}, 400);
							}
						});

						/**
						 * Back to top button - fade in and scroll functionality
						 */
						$(window).scroll(function () {
							if ($(window).scrollTop() > 300) {
								$('#back-to-top').fadeIn().addClass('show');
							} else {
								$('#back-to-top').fadeOut().removeClass('show');
							}
						});
						$('#back-to-top').click(function () {
							$("html, body").animate({
								scrollTop: 0,
							}, 400);
						});

						/*
            Columns
         */
						$('.wp-block-columns').each(function () {
							if (!$(this).hasClass('container')) {
								$(this).addClass('container');
							}
						});

						// FAQs page "Load More" buttons
						/* eslint-disable-next-line */
						if (ajax_localize.hasOwnProperty('ajax_url')) {
							$(".page-faqs .btn-faq-load-more").each(function () {

								var listItemHolder = $(this).siblings(".faq-accordion");

								// Hide the button if there aren't any more posts to load
								if ($(".list-items-loader", listItemHolder).length < 1) {
									$(this).hide();
								}

								$(this).on("click", function (evt) {

									evt.preventDefault();

									var loadMoreBtn = $(this);
									var listItemLoader = $(".list-items-loader", listItemHolder);

									// Save the button's text so we can restore it when removing the loading state
									var origButtonText = loadMoreBtn.html();

									if (loadMoreBtn.is(".loading")) {
										return false;
									}

									loadMoreBtn
										.attr("disabled", true)
										.addClass("loading")
										.text("Loading...");

									var action = "get_faqs";

									// Check if the list item loader has a custom action set
									if ((listItemLoader.data("action") !== undefined) && (listItemLoader.data("action") !== "")) {
										action = listItemLoader.data("action");
									}

									$.post(
										ajax_localize.ajax_url,
										{
											"action": action,
											"data": $.param(listItemLoader.data()),
										},
										function (data) {

											// Remove the existing list item loader since a new one will be included in the response
											listItemLoader.remove();

											// If new faq items were returned
											if (data) {

												loadMoreBtn
													.attr("disabled", false)
													.removeClass("loading")
													.html(origButtonText);

												var newFAQItems = $($.parseHTML(data));

												newFAQItems.filter(":not(.list-items-loader)").addClass("fade");

												listItemHolder.append(newFAQItems);

												// Wait a split second before adding class to fade in new faqs
												setTimeout(function () {
													newFAQItems.addClass("show");
												}, 100);

												// Look for the new list item loader tag
												// If it's not there, there aren't any more posts to show so hide the load more button
												if ($(".list-items-loader", listItemHolder).length < 1) {
													loadMoreBtn.remove();
												}

											} else {

												loadMoreBtn.fadeOut(400, function () {
													$(this).remove();
												});

												$(".no-more-results-msg").show();

											}

										}
									);

								});

							});
						}


						// Search tabs dropdown on mobile
						$("#search-category-dropdown-menu").on("change", function () {

							$("#search-page-filter-tabs").find(".nav-link.active").removeClass("active");
							$("#search-page-filter-tab-content").find(".tab-pane.active").removeClass("active");

							$("#search-page-filter-tabs").find(".nav-link[href='" + $(this).val() + "']").addClass("active");
							$($(this).val()).addClass("show active");

						});

						// FAQ Categories on mobile
						$("#faq-categories-dropdown").on("change", function () {

							if ($(this).val()) {
								window.location.href = $(this).val();
							}

							return false;

						});

						/**
						 * Add site class to body to test different sites
						 */
						// $('body').removeClass('tfi-site-canpar').addClass('tfi-site-ics');      // ICS
						// $('body').removeClass('tfi-site-canpar').addClass('tfi-site-loomis');   // Loomis
						// $('body').removeClass('tfi-site-canpar').addClass('tfi-site-tforce');   // TForce


						// TODO move this slick js logic to their JS files within blocks
						$('.slick-content').slick({
							infinite: true,
							speed: 300,
							slidesToShow: 5,
							slidesToScroll: 3,
							variableWidth: true,
							accessibility: false,
							responsive: [
								{
									breakpoint: 1024,
									settings: {
										slidesToShow: 3,
										slidesToScroll: 3,
										infinite: true,
										dots: true,
									},
								},
								{
									breakpoint: 600,
									settings: {
										slidesToShow: 2,
										slidesToScroll: 2,
									},
								},
								{
									breakpoint: 480,
									settings: {
										slidesToShow: 1,
										slidesToScroll: 1,
									},
								} ],
						});

						$('.slick-post-content').slick({
							infinite: true,
							speed: 300,
							slidesToShow: 2,
							slidesToScroll: 1,
							variableWidth: true,
							accessibility: false,
							responsive: [
								{
									breakpoint: 1024,
									settings: {
										slidesToShow: 2,
										slidesToScroll: 1,
										infinite: true,
										dots: true,
									},
								},
								{
									breakpoint: 600,
									settings: {
										slidesToShow: 2,
										slidesToScroll: 1,
									},
								},
								{
									breakpoint: 480,
									settings: {
										slidesToShow: 1,
										slidesToScroll: 1,
									},
								} ],
						});


						$('.video-modal').on('hidden.bs.modal', function () {
							$(".video-modal iframe").attr("src", $(".video-modal iframe").attr("src"));
							$('.embedded-video').trigger('pause');

						});


						// Update "Register" button in "My Portal" CTA to open register modal
						$(".cta-block:last-child .learn-more-link.two-links[href='#registration-modal']").each(function () {

							$(this).attr("data-toggle", "modal").attr("data-target", $(this).attr("href"));

						});

					});

					$.fn.observeScrollTopChanges = function (offset) {
						if ( offset === void 0 ) offset = 0;


						// Top and bottom margin values position marker element at the top of the box area of the current element, regardless of it's top margin
						// Need to increase bottom margin's negative value by 1px to compensate for marker's height
						// Essentially makes the marker invisible
						var marker = $("<div>").css({
							"margin-top": this.css("marginTop"),
							"margin-bottom": ((parseInt(this.css("marginTop")) + 1) * -1),
							"height": "1px",
						});

						if (parseInt(offset) > 0) {
							marker.css({
								"position": "relative",
								"top": ((parseInt(offset) + 1) * -1),
							});
						}

						// Add to DOM before element being observed
						// When marker is hidden beyond the top of the viewport, the target element will be touching the top of the viewport
						this.before(marker);

						// Create observer object
						var scrollTopObserver = new IntersectionObserver(function (entries) {

							// Width, height and scroll position of element being observed
							var targetBoundingRect = entries[0].boundingClientRect;
							// Width height and position of viewport
							var viewportBoundingRect = entries[0].rootBounds;

							// The element that should have the event triggered on it
							// Basically, the element that we want to know when it has reached the top of the page on scroll
							var evtTrigger = entries[0].target.nextSibling;

							var scrolledToTop = false;

							// If the entire marker element has scrolled beyond the top of the viewport
							if (targetBoundingRect.bottom < viewportBoundingRect.top) {

								scrolledToTop = true;

								// If the entire marker element is visible again
							} else if (targetBoundingRect.bottom >= viewportBoundingRect.top) {

								scrolledToTop = false;

							}

							// Create custom event and pass information about whether the element was just hit the top of the page or if it is being scrolled away
							var evt = new CustomEvent("scroll-top-change", {detail: {scrolledToTop: scrolledToTop}});

							evtTrigger.dispatchEvent(evt);

							// Threshold says how much of the element needs to be visible before checking for intersection
							// 0 means as soon as any part of the element is visible
						}, {threshold: [0]});

						// Start observing the marker element
						scrollTopObserver.observe(marker.get(0));

						// Return current element to allow for jQuery function chaining
						return this;

					};

					function normalizeOrangeCarouselHeights() {

						$(".orange-block.carousel-block").each(function () {

							var windowScreenSize = $(document).width();

							var origStyleAttr = "";

							var carouselItems = $(".carousel-item", this);
							var carouselItemCaptions = $(".carousel-item .carousel-caption", this);

							// Ensure caption isn't stretched so we can get actual height, especially on smaller screens
							if (windowScreenSize <= 991) {
								origStyleAttr = carouselItems.attr("style");
								carouselItems.attr("style", "min-height: 0 !important");
							}

							carouselItems.css("align-items", "flex-start");

							// Run through each item in jQuery set and get height and then pull basic array out result
							var carouselItemMinHeightsArr = carouselItemCaptions.map(function () {
								return $(this).outerHeight() + parseInt($(this).parent().css("padding-top")) + parseInt($(this).parent().css("padding-bottom"));
							}).get();

							// Restore original styles
							if (windowScreenSize <= 991) {
								carouselItems.attr("style", origStyleAttr);
							}

							carouselItems.css("align-items", "");

							var maxSlideHeight = Math.max.apply(null, carouselItemMinHeightsArr);

							if (windowScreenSize <= 991) {

								var imageHeight = $('.carousel-item .carousel-image').outerHeight();

								if ($('body').hasClass('tfi-site-ics')) {
									maxSlideHeight = imageHeight + maxSlideHeight + 30;
								} else {
									maxSlideHeight = imageHeight + maxSlideHeight + 50;
								}

							}

							// If the max slide height is less than the current height of the block, we don't need to update the min-height
							if (maxSlideHeight <= $(this).outerHeight()) {
								return false;
							}


							// Set the min-height of all .carousel-item
							if ($('body').hasClass('tfi-site-tforce')) {
								carouselItems.css("min-height", maxSlideHeight + 72);
							} else {
								carouselItems.attr('style', 'min-height: ' + maxSlideHeight + 'px !important');
							}

						});

					}

					// For testimonials
					//Normalize the internal carousel height
					function normalizeTestimonialCarouselHeights() {
						$('.testimonial-large-block').each(function () {
							var items = $('.carousel-item .card .main', this);
							// reset the height
							items.css('min-height', 0);
							// set the height
							var maxHeight = Math.max.apply(null,
								items.map(function () {
									return $(this).outerHeight()
								}).get());
							//Will fix the min-height of all .carousel-item
							items.css('min-height', maxHeight + 120 + 'px');
							$('.carousel-container').css('min-height', maxHeight + 120 + 'px');
							//To fix the height of the d-flex div to fix the content on the middle of that div
							//dflex.css('height', maxHeight + 'px');
						})

						$('.tfi-site-ics .testimonial-large-block').each(function () {
							var items = $('.carousel-item .card .main', this);
							// reset the height
							items.css('min-height', 0);
							// set the height
							var maxHeight = Math.max.apply(null,
								items.map(function () {
									return $(this).outerHeight()
								}).get());
							//Will fix the min-height of all .carousel-item
							items.css('min-height', maxHeight + 100 + 'px');
							$('.carousel-container').css('min-height', maxHeight + 100 + 'px');
							//To fix the height of the d-flex div to fix the content on the middle of that div
							//dflex.css('height', maxHeight + 'px');
						})

					}

					$(document).ready(function () {
						var intViewportHeight = window.innerWidth;
						//quick and dirty. This needs fixed
						$('.testimonial-large-block .carousel-container').each(function () {
							if(intViewportHeight > 900){
								$(this).css('min-height', '400px');
							}else{
								$(this).css('min-height', '500px');
							}
						})
					})

					$(window).on(
						'load resize orientationchange',
						normalizeTestimonialCarouselHeights);


					// This script will read the current url, if we have a FAQ condition, will open that accordion and scroll to that item
					if ($('body').hasClass('page-faqs')) {
						var urlParams = new URLSearchParams(window.location.search);
						if (urlParams) {
							var faqh = urlParams.get('faqh');
							$(".collapse[data-id='" + faqh + "']").collapse('toggle');
							Element.prototype.documentOffsetTop = function () {
								return this.offsetTop + (this.offsetParent ? this.offsetParent.documentOffsetTop() : 0);
							};
							//Not working and doesnt seem to be required
							// var top = document.querySelector("[data-id='" + faqh + "']").documentOffsetTop() - (window.innerHeight / 2);
							// window.scrollTo(0, top);
						}
					}


					// To open submenus on focus
					$('.dropdown.menu-item-has-children .nav-link').focus(function () {
						$(this).parent().addClass('open');
					});


					$('.dropdown.menu-item-has-children .nav-link').blur(function () {
						//$(this).parent().removeClass('open');
						// $(this).attr('aria-expanded',false);
						// console.log ('saiu da sessao')
					});

					$('.dropdown.menu-item-has-children .nav-link').click(function () {
						$('.open').removeClass('open');
						$(this).addClass('open');
					});


					/**
					 * All the Tforce API JS is below
					 */
					$(document).ready(function () {


						if (window.location.href.indexOf("tracking-portal") > -1) {
							$('body').addClass('tforce-tracking-portal');
						}
						// Search button animation

						$('button.float-right').click(function () {
							$(this).append('<div class="spinner"></div>');
							$(this).css('background-image', 'none');
						})

						if (window.location.pathname == '/support/faqs/') {

							var url_string = window.location.href;
							var url = new URL(url_string);
							var faqTitle = url.searchParams.get("faqh");

							if (faqTitle != null) {
								// Triggering load more button to get all faqs
								var triggerButton = setInterval(function triggerBtn() {
									$('.btn-load-more').trigger('click');
								}, 100);
							}
							var headerHeight = $('.site-header-wrap').height() + 70;
							var parentId = $(".accordion-title").filter(function () {
								return $(this).text() == faqTitle;
							}).parent().attr('id');

							if (parentId == undefined) {

								var scrolling = setInterval(function scrollToFaq() {
									// Check if all faq elements loaded
									if ($('.btn-load-more').length == 0) {
										var parent = $(".accordion-title").filter(function () {
											return $(this).text() == faqTitle;
										}).parent().attr('id');

										$('html, body').animate({scrollTop: $("#" + parent).offset().top - headerHeight}, 800);
										clearInterval(scrolling);
									}
								}, 1000);

							} else {
								$('html, body').animate({scrollTop: $("#" + parentId).offset().top - headerHeight}, 800);
								clearInterval(triggerButton);
							}
						}


						// Redirects for CTA single pages
						var urlParams = new URLSearchParams(window.location.search);
						if (urlParams.has('cta') == true) {
							window.location.href = '/';
						}

						// Adding class for no search results page
						if ($("body").hasClass("search-no-results")) {
							$("body").addClass("search-results");

						}
						// Alert Show / Hide
						// var alertSession = localStorage.getItem('alertShown');
						var alertCookie = getCookie('alert-closed');
						if (alertCookie === null) {
							$('#alert-close-btn').click(function () {
								setCookie('alert-closed', 'true', 0.25);
							});
						} else {
							$('.header-alert').remove();
						}
						//
						// if (alertSession === null) {
						//     $('#alert-close-btn').click(function () {
						//         localStorage.setItem('alertShown', 'true');
						//     });
						// } else {
						//     $('.header-alert').remove();
						// }


						// Bucket list click functions

						$("a.card.h-100").on('click', function (event) {
							if (this.hash !== "") {
								event.preventDefault();

								var headerHeight = $('.site-header-wrap').height();
								var hash = this.hash;
								var parentId = $(hash).parent().attr('id');
								$('html, body').animate({
									scrollTop: $("#" + parentId).offset().top - headerHeight,
								}, 800);
							}
						});


						if (window.location.pathname == '/solutions/') {

							var hashValue = window.location.hash;

							if (hashValue !== "") {
								var el = $('a[href="' + hashValue + '"]');
								el.trigger("click");
							}

						}

						// Alertb banner height for mobile views
						var height = [];
						$('.header-alert .carousel-item').each(function () {
							var $this = $(this);
							var sectionInnerHeight = $this.innerHeight();
							height.push(sectionInnerHeight);
							var maxHeight = Math.max.apply(Math, height);
							var width = $(window).width();
							if (width < 768) {
								$(this).height(maxHeight);
							}
						});


						$('#tracking-form').submit(function (event) {
							event.preventDefault();

							var tracking_numbers = $('#barcode').val();
							var results = $('.quick-tracker-block .tracking-results-container');

							$.ajax({
								type: 'POST',
								url: ajax_localize.ajax_url,
								dataType: "html", // add data type
								data: 'action=tforce_quick_trace&tracking_numbers=' + tracking_numbers,
								success: function (response) {
									$(results).html(response);

									$('.close-results').each(function () {
										$(this).click(function (event) {
											event.preventDefault();

											$(this).closest('.tforce-tracking-container').remove();

										});
									})



								},
							});

						})

						// if ($('body').hasClass('shipment-eta')) {
						//     validate_login();
						// }
						// if ($('body').hasClass('tracking-portal')) {
						//     validate_login();
						// }
						//
						// function validate_login() {
						//     var userId = getCookie('TFI-user-id');
						//     $.ajax({
						//         type: 'POST',
						//         url: ajax_localize.ajax_url,
						//         dataType: "html", // add data type
						//         data: 'action=check_login&user_id=' + userId + '&nonce=' + ajax_localize.nonce,
						//         success: function (response) {
						//             console.log('response - ' + response);
						//             if (response.includes('redirect_url:')) {
						//
						//                 eraseCookie('TFI-user-id');
						//                 eraseCookie('TFI-user-name');
						//                 eraseCookie('TFI-session');
						//
						//                 var redirect_url = response.substr(response.indexOf(':') + 1);
						//
						//                 console.log('redirect url : ' + redirect_url);
						//
						//                 window.location.href = redirect_url;
						//             }
						//         },
						//     });
						// }


						$('#tracking-login-form').submit(function (e) {
							e.preventDefault();

							var login = $('#login-name').val();
							var pass = $('#login-pass').val();
							//add / replace user session cookie
							var random_string = randomString(32);
							var user_unique_id = randomString(16);
							setCookie('TFI-session', random_string, 0.5);
							setCookie('TFI-user-id', user_unique_id, 0.5);
							setCookie('TFI-user-name', login, 0.5);

							//Send login details to server
							$.ajax({
								type: 'POST',
								url: ajax_localize.ajax_url,
								dataType: "html", // add data type
								data: 'action=tforce_user_login&login=' + login + '&pass=' + pass +
									'&nonce=' + ajax_localize.nonce + "&cookie=" + random_string + "&user_id=" + user_unique_id,
								success: function (response) {
									if (response.includes('redirect_url:')) {
										var redirect_url = response.substr(response.indexOf(':') + 1);

										window.location.href = redirect_url;
									} else {

										$('.login-form-container h6').html(response);

									}
								},

							});


						});

						$('#logout-button').click(function (event) {
							event.preventDefault();
							$.ajax({
								type: 'POST',
								url: ajax_localize.ajax_url,
								dataType: "html", // add data type
								data: 'action=tforce_logout&nonce=' + ajax_localize.nonce,
								success: function (response) {

									if (response.includes('redirect_url:')) {
										var redirect_url = response.substr(response.indexOf(':') + 1);

										eraseCookie('TFI-user-id');
										eraseCookie('TFI-user-name');
										eraseCookie('TFI-session');
										window.location.href = redirect_url;
									} else {
										window.location.href = '/tracking-portal-login';
									}

								},
							});

						})


						function randomString(length) {
							return Math.round((Math.pow(36, length + 1) - Math.random() * Math.pow(36, length))).toString(36).slice(1);
						}

						function setCookie(name, value, days) {
							var expires = "";
							if (days) {
								var date = new Date();
								date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
								expires = "; expires=" + date.toUTCString();
							}
							document.cookie = name + "=" + (value || "") + expires + "; path=/";
						}

						function getCookie(name) {
							var nameEQ = name + "=";
							var ca = document.cookie.split(';');
							for (var i = 0; i < ca.length; i++) {
								var c = ca[i];
								while (c.charAt(0) == ' ') { c = c.substring(1, c.length); }
								if (c.indexOf(nameEQ) == 0) { return c.substring(nameEQ.length, c.length); }
							}
							return null;
						}

						function eraseCookie(name) {
							document.cookie = name + '=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
						}


						$('#search-shipment').submit(function (event) {
							event.preventDefault();
							var type = 'ship';
							var numbers = $('#shipment-number').val();
							do_shipment_search(type, numbers)
						})
						$('#search-reference').submit(function (event) {
							event.preventDefault();
							var type = 'ref';
							var numbers = $('#reference-number').val();
							do_shipment_search(type, numbers)
						})
						$('#search-po').submit(function (event) {
							event.preventDefault();
							var type = 'po';
							var numbers = $('#po-number').val();
							do_shipment_search(type, numbers)
						})
						$('#search-date').submit(function (event) {
							event.preventDefault();
							var type = 'postal';
							var numbers = "";
							var from_date = $('#from-date-shipped').val();
							var to_date = $('#to-date-shipped').val();
							// var country = $('#country').val();
							var postal = $('#postal-code').val();


							do_shipment_search(type, numbers, from_date, to_date, postal);
						})


						function do_shipment_search(type, numbers, from_date, to_date, postal,oldParams,oldType) {
							if ( from_date === void 0 ) from_date = null;
							if ( to_date === void 0 ) to_date = null;
							if ( postal === void 0 ) postal = null;
							if ( oldParams === void 0 ) oldParams = null;
							if ( oldType === void 0 ) oldType = null;


							var user_name = getCookie('TFI-user-name');
							var user_id = getCookie('TFI-user-id');
							var cookie = getCookie('TFI-session');

							var params = '&numbers=' + numbers;
							if (type === 'postal') {

								params = '&FromDate=' + from_date + '&ToDate=' + to_date + '&PostalCode=' + postal;
							}

							var search_multi = 'false';
							$.ajax({
								type: 'POST',
								url: ajax_localize.ajax_url,
								dataType: "html", // add data type
								data: 'action=tforce_shipment_search&search-type=' + type + '&nonce=' + ajax_localize.nonce +
									'&unique_user_id=' + user_id + '&user-name=' + user_name + '&session-cookie=' + cookie + params,
								success: function (response) {

									if (response.includes('redirect_url:')) {
										var redirect_url = response.substr(response.indexOf(':') + 1);
										window.location.href = redirect_url;
									} else {

										if (response.includes('multi-results-csv')) {
											search_multi = true;
										}
										$('.search-results-container').removeClass('search-hidden')
										$('.shipment-search-results').html(response);

										$('.single-shipment-link').each(function(){
											$(this).attr('data-oldParams',params);
											$(this).attr('data-oldType',type);
										})

										if(oldParams !== null){
											$('#go-back-to-results').attr('data-params',oldParams);
											$('#go-back-to-results').attr('data-type',oldType);
										}

										if($('#go-back-to-results').length){
											$('#go-back-to-results').click(function(event){
												event.preventDefault();
												if($(this).attr('data-type') == 'postal'){
													var splitParams = $(this).attr('data-params').split('&');
													var fromDate = splitParams[1].substring(splitParams[1].indexOf('=') + 1);
													var toDate = splitParams[2].substring(splitParams[2].indexOf('=') + 1);
													var postal = splitParams[3].substring(splitParams[3].indexOf('=') + 1);
													do_shipment_search($(this).attr('data-type'), $(this).attr('data-params'),fromDate,toDate,postal);


												}else{
													do_shipment_search($(this).attr('data-type'), $(this).attr('data-params'));
												}

											})
										}

										$('.single-shipment-link').click(function (event) {
											event.preventDefault();
											var shipment = $(this).data('shipment');
											var oParams = $(this).attr('data-oldParams');
											var oType = $(this).attr('data-oldType');
											do_shipment_search('shipment', shipment,null,null,null,oParams,oType);

										})

										// var el = document.getElementById('search-outer-container');
										// //
										// el.scrollIntoView({block: "start"});

										var id = 'search-outer-container';
										var yOffset = -100;
										var element = document.getElementById(id);
										var y = element.getBoundingClientRect().top + window.pageYOffset + yOffset;

										window.scrollTo({top: y, behavior: 'smooth'});

										$('#print-results').click(function(e){
											e.preventDefault();
											printDiv();

										});
										another_request();
										collapseResults();
									}
								},
							});

							function another_request() {
								$.ajax({
									type: 'POST',
									url: ajax_localize.ajax_url,
									dataType: "html", // add data type
									data: 'action=tforce_shipment_search&search-type=' + type + '&nonce=' + ajax_localize.nonce +
										'&unique_user_id=' + user_id + '&user-name=' + user_name + '&session-cookie=' + cookie + params + '&csv=true',
									success: function (response) {
										if (search_multi === true) {
											if (response.includes('redirect_url:')) {
												var redirect_url = response.substr(response.indexOf(':') + 1);
												window.location.href = redirect_url;
											} else {
												if (document.getElementById('csv-export') !== null) {
													var downloadLink = document.getElementById('csv-export');
													var fileData = ['\ufeff' + response];

													var blobObject = new Blob(fileData, {
														type: "text/csv;charset=utf-8;",
													});

													var url = URL.createObjectURL(blobObject);
													downloadLink.href = url;
													downloadLink.download = "shipments.csv";

												}

											}
										}

									},
								});
							}


						}

						$('.csv-export').click(function () {
							create_csv();
						})

						//basically the same call as above
						function create_csv() {

						}

						function collapseResults() {
							var resultsContainer = $('.search-outer-container');
							var resultsSection = $('.results-overview-container.shipment-details');
							var showMoreBtn = $(resultsContainer).find('a.show-more');
							var highlightsHeight = $(resultsContainer).find('.highlights-section').height() + 213;

							resultsSection.css('height', highlightsHeight);
							resultsContainer.addClass('show');

							showMoreBtn.on('click', function (e) {
								e.preventDefault();
								resultsSection.toggleClass('open');
								$(this).toggleClass('open');

								if ($(this).hasClass('open')) {
									$(this).text('Show Less');
								} else {
									$(this).text('Show More');
								}
							});
						}


						//Surcharge History Ajax
						$('.history-list').change(function (event) {

							event.preventDefault();

							var startDate = $(this).find(':selected').attr('data-startDate');
							var endDate = $(this).find(':selected').attr('data-endDate');


							$.ajax({
								type: 'POST',
								url: ajax_localize.ajax_url,
								dataType: "html", // add data type
								data: 'action=get_surcharge_table&startDate=' + startDate +
									'&endDate=' + endDate +
									'&nonce=' + ajax_localize.nonce,
								success: function (response) {
									$('.history-result-container').html(response);
								},
							});
						});


						$('#search-eta').submit(function (event) {
							event.preventDefault();
							var user_name = getCookie('TFI-user-name');
							var user_id = getCookie('TFI-user-id');
							var cookie = getCookie('TFI-session');

							var from_postal = $('#from-postal-code').val();
							var to_postal = $('#to-postal-code').val();
							var pickup_date = $('#pick-up-date').val();
							var pickup_hour = $('#pick-up-hour').val();
							var pickup_minute = $('#pick-up-minute').val();
							var service_level = $('#service-level').val();

							var params = '&from-postal-code=' + from_postal + '&to-postal-code=' + to_postal + '&pick-up-date=' + pickup_date +
								'&pick-up-hour=' + pickup_hour + '&pick-up-minute=' + pickup_minute + '&service-level=' + service_level;


							$.ajax({
								type: 'POST',
								url: ajax_localize.ajax_url,
								dataType: "html", // add data type
								data: 'action=get_eta&nonce=' + ajax_localize.nonce +
									'&unique_user_id=' + user_id + '&user-name=' + user_name + '&session-cookie=' + cookie + params,
								success: function (response) {
									if (response.includes('redirect_url:')) {
										var redirect_url = response.substr(response.indexOf(':') + 1);
										window.location.href = redirect_url;
									} else {
										$('.search-outer-container').removeClass('search-hidden');
										$('.search-results-container').addClass('show');
										$('.eta-search-results').html(response);
									}

								},
							});
						});

						//print
						function printDiv() {

							window.print();

						}







					});

					$(document).on({
						ajaxStart: function () {
							$('.load-more-animation').addClass("loading");
							$('.app-button').attr('disabled', true);
						},
						ajaxStop: function () {
							$('.load-more-animation').removeClass("loading");
							$('.app-button').removeAttr('disabled')

							// var win = window.open('http://stackoverflow.com/', '_blank');
							// if (win) { //Browser has allowed it to be opened win.focus();
							//
							// }
						},
					});


					/* eslint-disable-next-line */
				})(jQuery, ajax_localize);
				/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! jquery */ "jquery")))

			/***/ }),

		/***/ "./styles/main.scss":
		/*!**************************!*\
  !*** ./styles/main.scss ***!
  \**************************/
		/*! no static exports found */
		/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

			/***/ }),

		/***/ 0:
		/*!**************************************************!*\
  !*** multi ./scripts/main.js ./styles/main.scss ***!
  \**************************************************/
		/*! no static exports found */
		/***/ (function(module, exports, __webpack_require__) {

			__webpack_require__(/*! ./scripts/main.js */"./scripts/main.js");
			module.exports = __webpack_require__(/*! ./styles/main.scss */"./styles/main.scss");


			/***/ }),

		/***/ "jquery":
		/*!*************************!*\
  !*** external "jQuery" ***!
  \*************************/
		/*! no static exports found */
		/***/ (function(module, exports) {

			module.exports = jQuery;

			/***/ })

		/******/ });
//# sourceMappingURL=main.js.map